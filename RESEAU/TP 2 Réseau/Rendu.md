# TP2 : Ethernet, IP, et ARP

# 0. Prérequis

# I. Setup IP

🌞 **Mettez en place une configuration réseau fonctionnelle entre les deux machines**

- vous renseignerez dans le compte rendu :
  - les deux IPs choisies, en précisant le masque

Commande utilisée:
<br>
netsh interface ip set address name="Ethernet" static 10.10.27.66 255.255.252.0

IPs choisies:
<br>
10.10.27.65
<br>
10.10.27.66

Masque:
<br>
255.255.252.0

  - l'adresse de réseau

10.10.24.0/22

  - l'adresse de broadcast

10.10.27.254

🌞 **Prouvez que la connexion est fonctionnelle entre les deux machines**

- un `ping` suffit !

commande: ping 10.10.27.65

Envoi d'une requête 'Ping'  10.10.27.65 avec 32 octets de données :
Réponse de 10.10.27.65 : octets=32 temps=2 ms TTL=128
Réponse de 10.10.27.65 : octets=32 temps=2 ms TTL=128
Réponse de 10.10.27.65 : octets=32 temps=2 ms TTL=128
Réponse de 10.10.27.65 : octets=32 temps=3 ms TTL=128

Statistiques Ping pour 10.10.27.65:
    Paquets : envoyés = 4, reçus = 4, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 2ms, Maximum = 3ms, Moyenne = 2ms
PS C:\WINDOWS\system32> netsh interface ip show config

🌞 **Wireshark it**

- `ping` ça envoie des paquets de type ICMP (c'est pas de l'IP, c'est un de ses frères)
  - les paquets ICMP sont encapsulés dans des trames Ethernet, comme les paquets IP
  - il existe plusieurs types de paquets ICMP, qui servent à faire des trucs différents
- **déterminez, grâce à Wireshark, quel type de paquet ICMP est envoyé par `ping`**
  - pour le ping que vous envoyez

type 8

  - et le pong que vous recevez en retour

  type 0

🦈 **PCAP qui contient les paquets ICMP qui vous ont permis d'identifier les types ICMP**

[Ping](./fichier/Ping.pcapng)

# II. ARP my bro

🌞 **Check the ARP table**

- utilisez une commande pour afficher votre table ARP

arp -a

- déterminez la MAC de votre binome depuis votre table ARP

04-42-1a-89-b3-1a

- déterminez la MAC de la *gateway* de votre réseau

00-c0-e7-e0-04-4e

  - celle de votre réseau physique, WiFi, genre YNOV, car il n'y en a pas dans votre ptit LAN

  - c'est juste pour vous faire manipuler un peu encore :)


🌞 **Manipuler la table ARP**

- utilisez une commande pour vider votre table ARP

arp -d

- prouvez que ça fonctionne en l'affichant et en constatant les changements

il n'y a que cette ligne afficher sur chaques interfaces:

Adresse Internet      Adresse physique      Type

  224.0.0.22            01-00-5e-00-00-16     statique

- ré-effectuez des pings, et constatez la ré-apparition des données dans la table ARP

réaparition de la donée suivante dans la table ARP:

10.10.27.65           04-42-1a-89-b3-1a     dynamique


🌞 **Wireshark it**

- vous savez maintenant comment forcer un échange ARP : il sufit de vider la table ARP et tenter de contacter quelqu'un, l'échange ARP se fait automatiquement
- mettez en évidence les deux trames ARP échangées lorsque vous essayez de contacter quelqu'un pour la "première" fois
  - déterminez, pour les deux trames, les adresses source et destination

- trame ARP broadcast:
adresse source: 10.10.27.66
adresse destination: 10.10.27.65

- trame ARP reply:
adresse source: 10.10.27.65
adresse destination: 10.10.27.66

  - déterminez à quoi correspond chacune de ces adresses

L'adresse source correspond à l'adresse qui envoi les paquets et l'adresse destination correspond à celui qui reçoit les paquets.


🦈 **PCAP qui contient les trames ARP**

[Ping2](./fichier/Ping2.pcapng)


# II.5 Interlude hackerzz

➜ **Le principe de l'attaque**

- on admet Alice, Bob et Eve, tous dans un LAN, chacun leur PC
- leur configuration IP est ok, tout va bien dans le meilleur des mondes
- **Eve 'lé pa jonti** *(ou juste un agent de la CIA)* : elle aimerait s'immiscer dans les conversations de Alice et Bob
  - pour ce faire, Eve va empoisonner la table ARP de Bob, pour se faire passer pour Alice
  - elle va aussi empoisonner la table ARP d'Alice, pour se faire passer pour Bob
  - ainsi, tous les messages que s'envoient Alice et Bob seront en réalité envoyés à Eve

➜ **La place de ARP dans tout ça**

- ARP est un principe de question -> réponse (broadcast -> *reply*)
- IL SE TROUVE qu'on peut envoyer des *reply* à quelqu'un qui n'a rien demandé :)
- il faut donc simplement envoyer :
  - une trame ARP reply à Alice qui dit "l'IP de Bob se trouve à la MAC de Eve" (IP B -> MAC E)
  - une trame ARP reply à Bob qui dit "l'IP de Alice se trouve à la MAC de Eve" (IP A -> MAC E)
- ha ouais, et pour être sûr que ça reste en place, il faut SPAM sa mum, genre 1 reply chacun toutes les secondes ou truc du genre
  - bah ui ! Sinon on risque que la table ARP d'Alice ou Bob se vide naturellement, et que l'échange ARP normal survienne
  - aussi, c'est un truc possible, mais pas normal dans cette utilisation là, donc des fois bon, ça chie, DONC ON SPAM


---

➜ J'peux vous aider à le mettre en place, mais **vous le faites uniquement dans un cadre privé, chez vous, ou avec des VMs**

➜ **Je vous conseille 3 machines Linux**, Alice Bob et Eve. La commande `[arping](https://sandilands.info/sgordon/arp-spoofing-on-wired-lan)` pourra vous carry : elle permet d'envoyer manuellement des trames ARP avec le contenu de votre choix.

GLHF.

# III. DHCP you too my brooo

DHCP pour Dynamic Host Configuration Protocol est notre p'tit pote qui nous file des IPs quand on arrive dans un réseau, parce que c'est chiant de le faire à la main :)

Quand on arrive dans un réseau, notre PC contacte un serveur DHCP, et récupère généralement 3 infos :

1. une IP à utiliser

2. l'adresse IP de la passerelle du réseau

3. l'adresse d'un serveur DNS joignable depuis ce réseau

🌞 **Wireshark it**

- identifiez les 4 trames DHCP lors d'un échange DHCP

DORA:

 1) Discover
<br>
2) Offer
<br>
3) Request
<br>
4) Acknowledge

  - mettez en évidence les adresses source et destination de chaque trame

Discover:
- adresse source: 0.0.0.0
- adresse destination: 255.255.255.255

Offer:
- adresse source: 10.33.19.254
- adresse destination: 10.33.16.246

Request:
- adresse source: 0.0.0.0
- adresse destination: 255.255.255.255

Acknowledge:
- adresse source: 10.33.19.246
- adresse destination: 10.33.16.246

- identifiez dans ces 4 trames les informations **1**, **2** et **3** dont on a parlé juste au dessus

**1)** 10.33.16.246
<br>
**2)** 10.33.19.254
<br>
**3)** 8.8.8.8
<br>
8.8.4.4
<br>
1.1.1.1

🦈 **PCAP qui contient l'échange DORA**

[DORA](./fichier/DORA.pcapng)
