# TP3 : On va router des trucs

## I. ARP

### 1. Echange ARP

🌞**Générer des requêtes ARP**

- effectuer un `ping` d'une machine à l'autre

commande: ping 10.3.1.12

résultat:

PING 10.3.1.12 (10.3.1.12) 56(84) bytes of data.
64 bytes from 10.3.1.12: icmp_seq=1 ttl=64 time=0.352 ms
64 bytes from 10.3.1.12: icmp_seq=2 ttl=64 time=0.382 ms
64 bytes from 10.3.1.12: icmp_seq=3 ttl=64 time=0.965 ms

- observer les tables ARP des deux machines

john:

commande: ip neigh show

résultat:

10.3.1.7 dev enp0s8 lladdr 0a:00:27:00:00:3c DELAY
10.3.1.12 dev enp0s8 lladdr 08:00:27:9d:96:57 STALE

marcel:

résultat:

10.3.1.11 dev enp0s8 lladdr 08:00:27:86:06:98 STALE
10.3.1.7 dev enp0s8 lladdr 0a:00:27:00:00:3c DELAY



- repérer l'adresse MAC de `john` dans la table ARP de `marcel` et vice-versa

Adresse MAC de john dans la table ARP de marcel:

08:00:27:9d:96:57

Adresse MAC de marcel dans la table ARP de john:

08:00:27:86:06:98

- prouvez que l'info est correcte (que l'adresse MAC que vous voyez dans la table est bien celle de la machine correspondante)



  - une commande pour voir la MAC de `marcel` dans la table ARP de `john`

"ip neigh show 10.3.1.12" (ip de john)

  - et une commande pour afficher la MAC de `marcel`, depuis `marcel`

ip neigh show dev enp0s8

### 2. Analyse de trames

🌞**Analyse de trames**

- utilisez la commande `tcpdump` pour réaliser une capture de trame

623 packets captured
625 packets received by filter
0 packets dropped by kernel

- videz vos tables ARP, sur les deux machines, puis effectuez un `ping`

sudo ip -s -s neigh flush all

🦈 **Capture réseau `tp3_arp.pcapng`** qui contient un ARP request et un ARP reply

[ARP request](./fichier/ARPrequest.pcapng)

[ARP reply](./fichier/ARPreply.pcapng)

## II. Routage

### 1. Mise en place du routage

🌞**Activer le routage sur le noeud `router`**

commandes:

sudo firewall-cmd --list-all
<br>
$ sudo firewall-cmd --get-active-zone

sudo firewall-cmd --add-masquerade --zone=public
<br>
sudo firewall-cmd --add-masquerade --zone=public --permanent

🌞**Ajouter les routes statiques nécessaires pour que `john` et `marcel` puissent se `ping`**

- il faut taper une commande `ip route add` pour cela, voir mémo

sudo ip route add 

- il faut ajouter une seule route des deux côtés

côté john:

sudo ip route add 10.3.2.12 via 10.3.1.254 dev enp0s8

côté marcel:

sudo ip route add 10.3.1.11 via 10.3.2.254 dev enp0s8

- une fois les routes en place, vérifiez avec un `ping` que les deux machines peuvent se joindre

--- 10.3.2.12 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1000ms
rtt min/avg/max/mdev = 0.949/1.532/2.115/0.583 ms


### 2. Analyse de trames

🌞**Analyse des échanges ARP**

- videz les tables ARP des trois noeuds

sudo ip neigh flush all

- effectuez un `ping` de `john` vers `marcel`

--- 10.3.2.12 ping statistics ---
5 packets transmitted, 5 received, 0% packet loss, time 4060ms
rtt min/avg/max/mdev = 0.891/1.391/2.010/0.465 ms

 - **le `tcpdump` doit être lancé sur la machine `john`**

- regardez les tables ARP des trois noeuds

john:
<br>
10.3.1.254 dev enp0s8 lladdr 08:00:27:0d:b0:c4 STALE
10.3.1.7 dev enp0s8 lladdr 0a:00:27:00:00:13 REACHABLE

marcel:
<br>
10.3.2.21 dev enp0s8 lladdr 0a:00:27:00:00:0f DELAY
10.3.2.254 dev enp0s8 lladdr 08:00:27:22:82:36 STALE

router:
<br>
10.3.1.11 dev enp0s8 lladdr 08:00:27:86:06:98 STALE
10.3.2.12 dev enp0s8 lladdr 08:00:27:9d:96:57 STALE

- essayez de déduire un peu les échanges ARP qui ont eu lieu

10.3.1.7 -> 10.3.1.11 -> 10.3.1.254 -> 10.3.2.254 -> 10.3.2.21 -> 10.3.2.12

- répétez l'opération précédente (vider les tables, puis `ping`), en lançant `tcpdump` sur `marcel`

40 packets captured
40 packets received by filter
0 packets dropped by kernel

- **écrivez, dans l'ordre, les échanges ARP qui ont eu lieu, puis le ping et le pong, je veux TOUTES les trames** utiles pour l'échange



Par exemple (copiez-collez ce tableau ce sera le plus simple) :

| ordre | type trame  | IP source | MAC source              | IP destination | MAC destination            |
|-------|-------------|-----------|-------------------------|----------------|----------------------------|
| 1     | Requête ARP | x        | `john` `08:00:27:22:82:36` | x              | Broadcast `00:00:00:00:00:00` |
| 2     | Réponse ARP | x         | `marcel` `0a:00:27:9d:96:57`                       | x              | `john` `08:00:27:22:82:36`    | 
| 3     | Ping        | 10.3.2.254        | `08:00:27:22:82:36` | 10.3.2.12              |  `08:00:27:9d:96:57`                          |
| 4     | Pong        | 10.3.2.12         |   `08:00:27:9d:96:57`                       | 10.3.2.254              | `08:00:27:22:82:36`                          |


🦈 **Capture réseau** 
<br>
[tp3_routage_marcel.pcapng](./fichier/tp3_routage_marcel.pcapng)

### 3. Accès internet

🌞**Donnez un accès internet à vos machines**

- ajoutez une carte NAT en 3ème inteface sur le `router` pour qu'il ait un accès internet
- ajoutez une route par défaut à `john` et `marcel`
  - vérifiez que vous avez accès internet avec un `ping`

--- 8.8.8.8 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2002ms
rtt min/avg/max/mdev = 22.908/50.203/80.989/23.839 ms

  - le `ping` doit être vers une IP, PAS un nom de domaine
- donnez leur aussi l'adresse d'un serveur DNS qu'ils peuvent utiliser

8.8.8.8

  - vérifiez que vous avez une résolution de noms qui fonctionne avec `dig`

  ; <<>> DiG 9.16.23-RH <<>> google.com
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 100
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 512
;; QUESTION SECTION:
;google.com.                    IN      A

;; ANSWER SECTION:
google.com.             14      IN      A       142.250.201.174

;; Query time: 28 msec
;; SERVER: 8.8.8.8#53(8.8.8.8)
;; WHEN: Fri Oct 28 09:26:25 CEST 2022
;; MSG SIZE  rcvd: 55

<<>> DiG 9.16.23-RH <<>>
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 42734
;; flags: qr rd ra ad; QUERY: 1, ANSWER: 13, AUTHORITY: 0, ADDITIONAL: 1

  - puis avec un `ping` vers un nom de domaine

  --- google.com ping statistics ---
11 packets transmitted, 11 received, 0% packet loss, time 10020ms
rtt min/avg/max/mdev = 23.166/42.737/70.891/17.135 ms

4 packets transmitted, 3 received, 25% packet loss, time 3005ms
rtt min/avg/max/mdev = 23.007/44.946/75.258/22.137 ms

🌞**Analyse de trames**

- effectuez un `ping 8.8.8.8` depuis `john`

--- 8.8.8.8 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2004ms
rtt min/avg/max/mdev = 27.106/55.732/70.256/20.242 ms

- capturez le ping depuis `john` avec `tcpdump`



- analysez un ping aller et le retour qui correspond et mettez dans un tableau :

| ordre | type trame | IP source          | MAC source              | IP destination | MAC destination |     |
|-------|------------|--------------------|-------------------------|----------------|-----------------|-----|
| 1     | ping       | `john` `10.3.1.11` | `john` `08:00:27:86:06:98` | `8.8.8.8`      | `08:00:27:0d:b0:c4`               |     |
| 2     | pong       | `8.8.8.8`                | `08:00:27:0d:b0:c4`                     | `john` `10.3.1.11`            | `john` `08:00:27:86:06:98`             |

🦈 **Capture réseau [tp3_routage_internet.pcapng](./fichier/tp3_routage_internet.pcapng)**

## III. DHCP

On reprend la config précédente, et on ajoutera à la fin de cette partie une 4ème machine pour effectuer des tests.

| Machine  | `10.3.1.0/24`              | `10.3.2.0/24` |
|----------|----------------------------|---------------|
| `router` | `10.3.1.254`               | `10.3.2.254`  |
| `john`   | `10.3.1.11`                | no            |
| `bob`    | oui mais pas d'IP statique | no            |
| `marcel` | no                         | `10.3.2.12`   |

```schema
   john               router              marcel
  ┌─────┐             ┌─────┐             ┌─────┐
  │     │    ┌───┐    │     │    ┌───┐    │     │
  │     ├────┤ho1├────┤     ├────┤ho2├────┤     │
  └─────┘    └─┬─┘    └─────┘    └───┘    └─────┘
   dhcp        │
  ┌─────┐      │
  │     │      │
  │     ├──────┘
  └─────┘
```

### 1. Mise en place du serveur DHCP

🌞**Sur la machine `john`, vous installerez et configurerez un serveur DHCP** (go Google "rocky linux dhcp server").

- installation du serveur sur `john`
- créer une machine `bob`
- faites lui récupérer une IP en DHCP à l'aide de votre serveur

> Il est possible d'utilise la commande `dhclient` pour forcer à la main, depuis la ligne de commande, la demande d'une IP en DHCP, ou renouveler complètement l'échange DHCP (voir `dhclient -h` puis call me et/ou Google si besoin d'aide).

🌞**Améliorer la configuration du DHCP**

- ajoutez de la configuration à votre DHCP pour qu'il donne aux clients, en plus de leur IP :
  - une route par défaut

option routers 10.3.1.254

  - un serveur DNS à utiliser

option domain-name-servers 8.8.8.8

- récupérez de nouveau une IP en DHCP sur `bob` pour tester :
  - `bob` doit avoir une IP

10.3.1.50

    - vérifier avec une commande qu'il a récupéré son IP

ip a

    - vérifier qu'il peut `ping` sa passerelle

--- 10.3.1.254 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1038ms
rtt min/avg/max/mdev = 0.326/0.387/0.449/0.061 ms

  - il doit avoir une route par défaut

10.3.1.254

    - vérifier la présence de la route avec une commande

commande: ip route show

résulat:
<br>
 default via 10.3.1.254 dev enp0s8 proto dhcp src 10.3.1.50 metric 100

    - vérifier que la route fonctionne avec un `ping` vers une IP

commande: ping 8.8.8.8

résultat:

--- 8.8.8.8 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3006ms
rtt min/avg/max/mdev = 22.927/24.685/27.160/1.564 ms

  - il doit connaître l'adresse d'un serveur DNS pour avoir de la résolution de noms
    - vérifier avec la commande `dig` que ça fonctionne

commande: dig 8.8.8.8

résultat:

; <<>> DiG 9.16.23-RH <<>> 8.8.8.8
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NXDOMAIN, id: 28970
;; flags: qr rd ra ad; QUERY: 1, ANSWER: 0, AUTHORITY: 1, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 512
;; QUESTION SECTION:
;8.8.8.8.                       IN      A

;; AUTHORITY SECTION:
.                       86163   IN      SOA     a.root-servers.net. nstld.verisign-grs.com. 2022102800 1800 900 604800 86400

;; Query time: 26 msec
;; SERVER: 8.8.8.8#53(8.8.8.8)
;; WHEN: Fri Oct 28 11:36:40 CEST 2022
;; MSG SIZE  rcvd: 111

    - vérifier un `ping` vers un nom de domaine

commande: ping google.com

résultat:

--- google.com ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2005ms
rtt min/avg/max/mdev = 22.243/23.352/24.580/0.957 ms

### 2. Analyse de trames

🌞**Analyse de trames**

- lancer une capture à l'aide de `tcpdump` afin de capturer un échange DHCP


- demander une nouvelle IP afin de générer un échange DHCP

commandes:

$ ip addr del 10.3.1.50

$ ip addr add 10.3.1.55

- exportez le fichier `.pcapng`

🦈 **Capture réseau [tp3_dhcp.pcapng](./fichier/dhcp.pcapng)**

