# TP4 : TCP, UDP et services réseau


# I. First steps

🌞 **Déterminez, pour ces 5 applications, si c'est du TCP ou de l'UDP**

- avec Wireshark, on va faire les chirurgiens réseau
- déterminez, pour chaque application :
  - IP et port du serveur auquel vous vous connectez
  - le port local que vous ouvrez pour vous connecter

APP 1:
-> lancement ck3.exe

-> se connecte au serveur ip 13.107.42.12 et port 443

-> ouvre le port local 57417 pour se connecter au serveur

-> **Capture réseau [ck3.pcapng](./fichier/ck3.pcapng)**

-> commande: netstat -b-n-p tcp -a

resultat:     TCP    10.33.16.246:57376     13.107.42.12:443       TIME_WAIT
  TCP    10.33.16.246:57377     13.107.42.12:443       TIME_WAIT
  TCP    10.33.16.246:57378     51.104.164.114:443     TIME_WAIT
  TCP    10.33.16.246:57394     54.197.174.6:443       CLOSE_WAIT
 [ck3.exe]

APP 2:
-> lancement Overwatch.exe

-> se connecte au serveur ip 5.42.175.64 et port 26505

-> ouvre le port local 54351 pour se connecter au serveur

-> **Capture réseau [Overwatch.pcapng](./fichier/Overwatch.pcapng)**

-> commande: netstat -b-n-p udp -a

resultat:   UDP    0.0.0.0:54351          *:*
 [Overwatch.exe]

APP 3:

-> lancement FactoryGame-Win64-Shipping.exe + connection sur un serveur existant (sur le jeu)

-> se connecte au serveur ip 173.249.54.166 et port 7777

-> ouvre le port local 5145 pour se connecter au serveur

-> **Capture réseau [FactoryGame.pcapng](./fichier/FactoryGame.pcapng)**

-> commande: netstat -b-n-p udp -a

resultat: UDP    0.0.0.0:51455          *:*
 [FactoryGame-Win64-Shipping.exe]

APP 4:
-> lancement PrimeVideo.exe puis visionnage vidéo

-> se connecte au serveur ip 65.9.66.32 et port 443

-> ouvre le port local 59674 pour se connecter au serveur

-> **Capture réseau [PrimeVideo.pcapng](./fichier/PrimeVideo.pcapng)**

-> commande: netstat -b-n-p tcp -a

 resultat: TCP    10.33.16.246:59674     65.9.66.32:443         ESTABLISHED
 [PrimeVideo.exe]

APP 5:
-> lancement Maps.exe

-> se connecte au serveur ip 2.16.209.47 et port 443

-> ouvre le port local 5145 pour se connecter au serveur

-> **Capture réseau [Maps.pcapng](./fichier/Maps.pcapng)**

-> commande: netstat -b-n-p tcp -a

resultat:   TCP    10.33.16.246:64876     2.16.209.47:443        ESTABLISHED
 [Maps.exe]


🌞 **Demandez l'avis à votre OS**

- votre OS est responsable de l'ouverture des ports, et de placer un programme en "écoute" sur un port
- il est aussi responsable de l'ouverture d'un port quand une application demande à se connecter à distance vers un serveur
- bref il voit tout quoi
- utilisez la commande adaptée à votre OS pour repérer, dans la liste de toutes les connexions réseau établies, la connexion que vous voyez dans Wireshark, pour chacune des 5 applications

**Il faudra ajouter des options adaptées aux commandes pour y voir clair. Pour rappel, vous cherchez des connexions TCP ou UDP.**

```
# MacOS
$ netstat

# GNU/Linux
$ ss

# Windows
$ netstat
```

🦈🦈🦈🦈🦈 **Bah ouais, captures Wireshark à l'appui évidemment.** Une capture pour chaque application, qui met bien en évidence le trafic en question.

# II. Mise en place

## 1. SSH

🖥️ **Machine `node1.tp4.b1`**

- n'oubliez pas de dérouler la checklist (voir [les prérequis du TP](#0-prérequis))
- donnez lui l'adresse IP `10.4.1.11/24`

`sudo nano ifcfg-enp0s8` dans `/etc/sysconf/network-scripts`

Connectez-vous en SSH à votre VM.

-> fonctionnel

🌞 **Examinez le trafic dans Wireshark**

- **déterminez si SSH utilise TCP ou UDP**

ssh utilise TCP

  - pareil réfléchissez-y deux minutes, logique qu'on utilise pas UDP non ?
- **repérez le *3-Way Handshake* à l'établissement de la connexion**
  - c'est le `SYN` `SYNACK` `ACK`
- **repérez du trafic SSH**
- **repérez le FIN ACK à la fin d'une connexion**
- entre le *3-way handshake* et l'échange `FIN`, c'est juste une bouillie de caca chiffré, dans un tunnel TCP

🌞 **Demandez aux OS**

- repérez, avec une commande adaptée (`netstat` ou `ss`), la connexion SSH depuis votre machine

commande: netstat -n -b

résultat:   TCP    10.4.1.28:49758        10.4.1.11:22           ESTABLISHED
 [ssh.exe]

- ET repérez la connexion SSH depuis votre VM

commande: ss

résulat: tcp   ESTAB 0      0                        10.4.1.11:22        10.4.1.28:49758

🦈 **Je veux une capture clean avec le 3-way handshake, un peu de trafic au milieu et une fin de connexion**

**Capture réseau [3-way_handshake_établissement_connecxion.pcapng](./fichier/3-way_handshake_établissement_connecxion.pcapng)**

## 2. Routage

Ouais, un peu de répétition, ça fait jamais de mal. On va créer une machine qui sera notre routeur, et **permettra à toutes les autres machines du réseau d'avoir Internet.**

🖥️ **Machine `router.tp4.b1`**

- n'oubliez pas de dérouler la checklist (voir [les prérequis du TP](#0-prérequis))
- donnez lui l'adresse IP `10.4.1.254/24` sur sa carte host-only
- ajoutez-lui une carte NAT, qui permettra de donner Internet aux autres machines du réseau
- référez-vous au TP précédent

> Rien à remettre dans le compte-rendu pour cette partie.

# III. DNS

## 1. Présentation

Un serveur DNS est un serveur qui est capable de répondre à des requêtes DNS.

Une requête DNS est la requête effectuée par une machine lorsqu'elle souhaite connaître l'adresse IP d'une machine, lorsqu'elle connaît son nom.

Par exemple, si vous ouvrez un navigateur web et saisissez `https://www.google.com` alors une requête DNS est automatiquement effectuée par votre PC pour déterminez à quelle adresse IP correspond le nom `www.google.com`.

> La partie `https://` ne fait pas partie du nom de domaine, ça indique simplement au navigateur la méthode de connexion. Ici, c'est HTTPS.

Dans cette partie, on va monter une VM qui porte un serveur DNS. Ce dernier répondra aux autres VMs du LAN quand elles auront besoin de connaître des noms. Ainsi, ce serveur pourra :

- résoudre des noms locaux
  - vous pourrez `ping node1.tp4.b1` et ça fonctionnera
  - mais aussi `ping www.google.com` et votre serveur DNS sera capable de le résoudre aussi

*Dans la vraie vie, il n'est pas rare qu'une entreprise gère elle-même ses noms de domaine, voire gère elle-même son serveur DNS. C'est donc du savoir ré-utilisable pour tous qu'on voit ici.*

> En réalité, ce n'est pas votre serveur DNS qui pourra résoudre `www.google.com`, mais il sera capable de *forward* (faire passer) votre requête à un autre serveur DNS qui lui, connaît la réponse.

![Haiku DNS](./pics/haiku_dns.png)

## 2. Setup

🖥️ **Machine `dns-server.tp4.b1`**

- n'oubliez pas de dérouler la checklist (voir [les prérequis du TP](#0-prérequis))
- donnez lui l'adresse IP `10.4.1.201/24`

Installation du serveur DNS :

```bash
# assurez-vous que votre machine est à jour
$ sudo dnf update -y

# installation du serveur DNS, son p'tit nom c'est BIND9
$ sudo dnf install -y bind bind-utils
```

La configuration du serveur DNS va se faire dans 3 fichiers essentiellement :

- **un fichier de configuration principal**
  - `/etc/named.conf`
  - on définit les trucs généraux, comme les adresses IP et le port où on veu écouter
  - on définit aussi un chemin vers les autres fichiers, les fichiers de zone
- **un fichier de zone**
  - `/var/named/tp4.b1.db`
  - je vous préviens, la syntaxe fait mal
  - on peut y définir des correspondances `IP ---> nom`
- **un fichier de zone inverse**
  - `/var/named/tp4.b1.rev`
  - on peut y définir des correspondances `nom ---> IP`

➜ **Allooooons-y, fichier de conf principal**

```bash
# éditez le fichier de config principal pour qu'il ressemble à :
$ sudo cat /etc/named.conf
options {
        listen-on port 53 { 127.0.0.1; any; };
        listen-on-v6 port 53 { ::1; };
        directory       "/var/named";
[...]
        allow-query     { localhost; any; };
        allow-query-cache { localhost; any; };

        recursion yes;
[...]
# référence vers notre fichier de zone
zone "tp4.b1" IN {
     type master;
     file "tp4.b1.db";
     allow-update { none; };
     allow-query {any; };
};
# référence vers notre fichier de zone inverse
zone "1.4.10.in-addr.arpa" IN {
     type master;
     file "tp4.b1.rev";
     allow-update { none; };
     allow-query { any; };
};
```

➜ **Et pour les fichiers de zone**

```bash
# Fichier de zone pour nom -> IP

$ sudo cat /var/named/tp4.b1.db

$TTL 86400
@ IN SOA dns-server.tp4.b1. admin.tp4.b1. (
    2019061800 ;Serial
    3600 ;Refresh
    1800 ;Retry
    604800 ;Expire
    86400 ;Minimum TTL
)

; Infos sur le serveur DNS lui même (NS = NameServer)
@ IN NS dns-server.tp4.b1.

; Enregistrements DNS pour faire correspondre des noms à des IPs
dns-server IN A 10.4.1.201
node1      IN A 10.4.1.11
```

```bash
# Fichier de zone inverse pour IP -> nom

$ sudo cat /var/named/tp4.b1.rev

$TTL 86400
@ IN SOA dns-server.tp4.b1. admin.tp4.b1. (
    2019061800 ;Serial
    3600 ;Refresh
    1800 ;Retry
    604800 ;Expire
    86400 ;Minimum TTL
)

; Infos sur le serveur DNS lui même (NS = NameServer)
@ IN NS dns-server.tp4.b1.

;Reverse lookup for Name Server
201 IN PTR dns-server.tp4.b1.
11 IN PTR node1.tp4.b1.
```

➜ **Une fois ces 3 fichiers en place, démarrez le service DNS**

```bash
# Démarrez le service tout de suite
$ sudo systemctl start named

# Faire en sorte que le service démarre tout seul quand la VM s'allume
$ sudo systemctl enable named

# Obtenir des infos sur le service
$ sudo systemctl status named

# Obtenir des logs en cas de probème
$ sudo journalctl -xe -u named
```

🌞 **Dans le rendu, je veux**

- un `cat` des fichiers de conf

commande: sudo cat /etc/named.conf

résultat: //
// named.conf
//
// Provided by Red Hat bind package to configure the ISC BIND named(8) DNS
// server as a caching only nameserver (as a localhost DNS resolver only).
//
// See /usr/share/doc/bind*/sample/ for example named configuration files.
//

options {
        listen-on port 53 { 127.0.0.1; any; };
        listen-on-v6 port 53 { ::1; };
        directory       "/var/named";
        dump-file       "/var/named/data/cache_dump.db";
        statistics-file "/var/named/data/named_stats.txt";
        memstatistics-file "/var/named/data/named_mem_stats.txt";
        secroots-file   "/var/named/data/named.secroots";
        recursing-file  "/var/named/data/named.recursing";


        /*
         - If you are building an AUTHORITATIVE DNS server, do NOT enable recursion.
         - If you are building a RECURSIVE (caching) DNS server, you need to enable
           recursion.
         - If your recursive DNS server has a public IP address, you MUST enable access
           control to limit queries to your legitimate users. Failing to do so will
           cause your server to become part of large scale DNS amplification
           attacks. Implementing BCP38 within your network would greatly
           reduce such attack surface
        */

        allow-query     { localhost; any; };
        allow-query-cache { localhost; any; };


        recursion yes;

        dnssec-validation yes;

        managed-keys-directory "/var/named/dynamic";
        geoip-directory "/usr/share/GeoIP";

        pid-file "/run/named/named.pid";
        session-keyfile "/run/named/session.key";

        /* https://fedoraproject.org/wiki/Changes/CryptoPolicy */
        include "/etc/crypto-policies/back-ends/bind.config";
};

logging {
        channel default_debug {
                file "data/named.run";
                severity dynamic;
        };
};

zone "tp4.b1" IN {
        type master;
        file "tp4.b1.db";
        allow-update { none; };
        allow-query { any; };
};

zone "1.4.10.in-addr.arpa" IN {
        type master;
        file "tp4.b1.rev";
        allow-update { none; };
        allow-query { any; };
};

include "/etc/named.rfc1912.zones";
include "/etc/named.root.key";

commande: sudo cat /var/named/tp4.b1.db

résultat: $TTL 86400
@ IN SOA dns-server.tp4.b1. admin.tp4.b1. (
    2019061800 ;Serial
    3600 ;Refresh
    1800 ;Retry
    604800 ;Expire
    86400 ;Minimum TTL
)

; Infos sur le serveur DNS lui même (NS = NameServer)
@ IN NS dns-server.tp4.b1.

; Enregistrements DNS pour faire correspondre des noms à des IPs
dns-server IN A 10.4.1.201
node1      IN A 10.4.1.11

commande: /var/named/tp4.b1.rev

résultat: $TTL 86400
@ IN SOA dns-server.tp4.b1. admin.tp4.b1. (
    2019061800 ;Serial
    3600 ;Refresh
    1800 ;Retry
    604800 ;Expire
    86400 ;Minimum TTL
)

; Infos sur le serveur DNS lui même (NS = NameServer)
@ IN NS dns-server.tp4.b1.

;Reverse lookup for Name Server
201 IN PTR dns-server.tp4.b1.
11 IN PTR node1.tp4.b1.


- un `systemctl status named` qui prouve que le service tourne bien

commande: systemctl status named

résultat: named.service - Berkeley Internet Name Domain (DNS)
     Loaded: loaded (/usr/lib/systemd/system/named.service; enabled; vendor preset: disabled)
     Active: active (running) since Thu 2022-11-10 12:09:04 CET; 3min 49s ago
   Main PID: 33445 (named)
      Tasks: 5 (limit: 5905)
     Memory: 18.7M
        CPU: 38ms
     CGroup: /system.slice/named.service
             └─33445 /usr/sbin/named -u named -c /etc/named.conf

Nov 10 12:09:04 dns-server named[33445]: network unreachable resolving './NS/IN': 2001:503:c27::2:30#53
Nov 10 12:09:04 dns-server named[33445]: zone 1.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.ip6>
Nov 10 12:09:04 dns-server named[33445]: zone tp4.b1/IN: loaded serial 2019061800
Nov 10 12:09:04 dns-server named[33445]: zone 1.0.0.127.in-addr.arpa/IN: loaded serial 0
Nov 10 12:09:04 dns-server named[33445]: zone localhost/IN: loaded serial 0
Nov 10 12:09:04 dns-server named[33445]: all zones loaded
Nov 10 12:09:04 dns-server systemd[1]: Started Berkeley Internet Name Domain (DNS).
Nov 10 12:09:04 dns-server named[33445]: running
Nov 10 12:09:04 dns-server named[33445]: managed-keys-zone: Initializing automatic trust anchor management for zo>
Nov 10 12:09:04 dns-server named[33445]: resolver priming query complete

- une commande `ss` qui prouve que le service écoute bien sur un port

commande: ss -laputen

résultat:

tcp          LISTEN        0             10                           [::1]:53                         [::]:*
 uid:25 ino:51426 sk:b cgroup:/system.slice/named.service v6only:1 <->

 tcp          LISTEN        0             10                       127.0.0.1:53                      0.0.0.0:*            uid:25 ino:51422 sk:7 cgroup:/system.slice/named.service <->

 udp          UNCONN        0             0                            [::1]:53                         [::]:*            uid:25 ino:51425 sk:5 cgroup:/system.slice/named.service v6only:1 <->
tcp          LISTEN        0             10                      10.4.1.201:53                      0.0.0.0:*            uid:25 ino:51424 sk:6 cgroup:/system.slice/named.service <->

udp          UNCONN        0             0                       10.4.1.201:53                      0.0.0.0:*            uid:25 ino:51423 sk:2 cgroup:/system.slice/named.service <->
udp          UNCONN        0             0                        127.0.0.1:53                      0.0.0.0:*            uid:25 ino:51421 sk:3 cgroup:/system.slice/named.service <->

🌞 **Ouvrez le bon port dans le firewall**

- grâce à la commande `ss` vous devrez avoir repéré sur quel port tourne le service

Le service tourne sur le port 53

  - vous l'avez écrit dans la conf aussi toute façon :)
- ouvrez ce port dans le firewall de la machine 
`dns-server.tp4.b1` (voir le mémo réseau Rocky)

commande: sudo firewall-cmd --add-port=53/tcp --permanent

## 3. Test

🌞 **Sur la machine `node1.tp4.b1`**

- configurez la machine pour qu'elle utilise votre serveur DNS quand elle a besoin de résoudre des noms
- assurez vous que vous pouvez :
  - résoudre des noms comme `node1.tp4.b1` et `dns-server.tp4.b1`

[dora@node1 ~]$ dig node1.tp4.b1

; <<>> DiG 9.16.23-RH <<>> node1.tp4.b1
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 8870
;; flags: qr aa rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1232
; COOKIE: 2fd15d2cbceb0da501000000636ce01710b70ae2226f6c5c (good)
;; QUESTION SECTION:
;node1.tp4.b1.                  IN      A

;; ANSWER SECTION:
node1.tp4.b1.           86400   IN      A       10.4.1.11

;; Query time: 0 msec
;; SERVER: 10.4.1.201#53(10.4.1.201)
;; WHEN: Thu Nov 10 12:27:19 CET 2022
;; MSG SIZE  rcvd: 85


[dora@node1 ~]$ dig dns-server.tp4.b1

; <<>> DiG 9.16.23-RH <<>> dns-server.tp4.b1
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 19808
;; flags: qr aa rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1232
; COOKIE: 7139677cf27a32a001000000636ce05663afd4e3fb35363f (good)
;; QUESTION SECTION:
;dns-server.tp4.b1.             IN      A

;; ANSWER SECTION:
dns-server.tp4.b1.      86400   IN      A       10.4.1.201

;; Query time: 3 msec
;; SERVER: 10.4.1.201#53(10.4.1.201)
;; WHEN: Thu Nov 10 12:28:22 CET 2022
;; MSG SIZE  rcvd: 90



  - mais aussi des noms comme `www.google.com`

  [dora@node1 ~]$ dig www.google.com

; <<>> DiG 9.16.23-RH <<>> www.google.com
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 34299
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1232
; COOKIE: 5e39d6002f1b587701000000636ce06f8432a9c976655ffb (good)
;; QUESTION SECTION:
;www.google.com.                        IN      A

;; ANSWER SECTION:
www.google.com.         300     IN      A       216.58.214.164

;; Query time: 187 msec
;; SERVER: 10.4.1.201#53(10.4.1.201)
;; WHEN: Thu Nov 10 12:28:47 CET 2022
;; MSG SIZE  rcvd: 87


🌞 **Sur votre PC**

- utilisez une commande pour résoudre le nom `node1.tp4.b1` en utilisant `10.4.1.201` comme serveur DNS

PS C:\Users\ZerBr> nslookup node1.tp4.b1 10.4.1.201
Serveur :   dns-server.tp4.b1
Address:  10.4.1.201

Nom :    node1.tp4.b1
Address:  10.4.1.11

> Le fait que votre serveur DNS puisse résoudre un nom comme `www.google.com`, ça s'appelle la récursivité et c'est activé avec la ligne `recursion yes;` dans le fichier de conf.

🦈 **Capture d'une requête DNS vers le nom `node1.tp4.b1` ainsi que la réponse**

**Capture réseau [node1.tp4.b1.pcapng](./fichier/node1.tp4.b1.pcapng)**