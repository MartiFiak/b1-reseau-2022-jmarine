# TP2 : Appréhender l'environnement Linux

# I. Service SSH

Le service SSH est déjà installé sur la machine, et il est aussi déjà démarré par défaut, c'est Rocky qui fait ça nativement.

## 1. Analyse du service

On va, dans cette première partie, analyser le service SSH qui est en cours d'exécution.

🌞 **S'assurer que le service `sshd` est démarré**

- avec une commande `systemctl status`

[dora@LinuxOne ~]$ sudo systemctl is-active sshd

résultat: active

🌞 **Analyser les processus liés au service SSH**

- afficher les processus liés au service `sshd`
  - vous pouvez afficher la liste des processus en cours d'exécution avec une commande `ps`

commande: ps -ef

  - pour le compte-rendu, vous devez filtrer la sortie de la commande en ajoutant `| grep <TEXTE_RECHERCHE>` après une commande
    - exemple :

```bash
# Exemple de manipulation de | grep

# admettons un fichier texte appelé "fichier_demo"
# on peut afficher son contenu avec la commande cat :
$ cat fichier_demo
bob a un chapeau rouge
emma surfe avec un dinosaure
eve a pas toute sa tête

# il est possible de filtrer la sortie de la commande cat pour afficher uniquement certaines lignes
$ cat fichier_demo | grep emma
emma surfe avec un dinosaure

$ cat fichier_demo | grep bob
bob a un chapeau rouge
```

```
[dora@LinuxOne ~]$ ps -ef | grep sshd
root         695       1  0 11:29 ?        00:00:00 sshd: /usr/sbin/sshd -D [listener] 0 of 10-100 startups
root         956     695  0 11:32 ?        00:00:00 sshd: dora [priv]
dora         960     956  0 11:32 ?        00:00:00 sshd: dora@pts/0
dora        1290     961  0 11:53 pts/0    00:00:00 grep --color=auto sshd
```


🌞 **Déterminer le port sur lequel écoute le service SSH**

- avec une commande `ss`

sudo ss -alnpt


- isolez les lignes intéressantes avec un `| grep <TEXTE>`
```
[dora@LinuxOne ~]$ sudo ss -alnpt | grep 22
LISTEN 0      128          0.0.0.0:22        0.0.0.0:*    users:(("sshd",pid=695,fd=3))
LISTEN 0      128             [::]:22           [::]:*    users:(("sshd",pid=695,fd=4))
```

🌞 **Consulter les logs du service SSH**

- les logs du service sont consultables avec une commande `journalctl`

journalctl -xe -u -sshd


- un fichier de log qui répertorie toutes les tentatives de connexion SSH existe
  - il est dans le dossier `/var/log`
  - utilisez une commande `tail` pour visualiser les 10 dernière lignes de ce fichier

```
  [dora@LinuxOne log]$ tail -n 10 dnf.log
2022-12-05T12:02:07+0100 DEBUG countme: event triggered for extras: bucket 1
2022-12-05T12:02:07+0100 DEBUG reviving: failed for 'extras', mismatched repomd.
2022-12-05T12:02:07+0100 DEBUG repo: downloading from remote: extras
2022-12-05T12:02:07+0100 DEBUG countme: no event for extras: window already counted
2022-12-05T12:02:08+0100 DEBUG extras: using metadata from Wed 23 Nov 2022 11:58:01 PM CET.
2022-12-05T12:02:08+0100 DEBUG User-Agent: constructed: 'libdnf (Rocky Linux 9.0; generic; Linux.x86_64)'
2022-12-05T12:02:08+0100 DDEBUG timer: sack setup: 8393 ms
2022-12-05T12:02:08+0100 DEBUG Completion plugin: Generating completion cache...
2022-12-05T12:02:08+0100 INFO Metadata cache created.
2022-12-05T12:02:08+0100 DDEBUG Cleaning up.
```

![When she tells you](./pics/when_she_tells_you.png)

## 2. Modification du service

Dans cette section, on va aller visiter et modifier le fichier de configuration du serveur SSH.

Comme tout fichier de configuration, celui de SSH se trouve dans le dossier `/etc/`.

Plus précisément, il existe un sous-dossier `/etc/ssh/` qui contient toute la configuration relative au protocole SSH

🌞 **Identifier le fichier de configuration du serveur SSH**

```
[dora@LinuxOne ssh]$ ls -al | grep ssh | grep config
-rw-r--r--.  1 root root       1921 Sep 20 20:46 ssh_config
drwxr-xr-x.  2 root root         28 Oct 14 11:20 ssh_config.d
```

🌞 **Modifier le fichier de conf**

- exécutez un `echo $RANDOM` pour demander à votre shell de vous fournir un nombre aléatoire

```
[dora@LinuxOne ssh]$ echo $RANDOM
19902
```

  - simplement pour vous montrer la petite astuce et vous faire manipuler le shell :)
- changez le port d'écoute du serveur SSH pour qu'il écoute sur ce numéro de port

```
[dora@LinuxOne ssh]$ sudo nano sshd_config
```



  - dans le compte-rendu je veux un `cat` du fichier de conf

```
[dora@LinuxOne ssh]$ sudo cat sshd_config | grep Port
Port 19902
```

  - filtré par un `| grep` pour mettre en évidence la ligne que vous avez modifié
- gérer le firewall
  - fermer l'ancien port

```
[dora@LinuxOne ssh]$ sudo firewall-cmd --remove-port=22/tcp --permanent
success
```

  - ouvrir le nouveau port

```
[dora@LinuxOne ssh]$ sudo firewall-cmd --add-port=19902/tcp --permanent
success
```

  - vérifier avec un `firewall-cmd --list-all` que le port est bien ouvert
    - vous filtrerez la sortie de la commande avec un `| grep TEXTE`

```
[dora@LinuxOne ssh]$ sudo firewall-cmd --list-all | grep 19902
  ports: 19902/tcp
```

🌞 **Redémarrer le service**

- avec une commande `systemctl restart`

```
[dora@LinuxOne ~]$ sudo systemctl restart sshd
```

🌞 **Effectuer une connexion SSH sur le nouveau port**

- depuis votre PC
- il faudra utiliser une option à la commande `ssh` pour vous connecter à la VM

```
ZerBr@MartyFiakoTron MINGW64 ~ (master)
$ ssh dora@10.4.1.70 -p 19902
```

✨ **Bonus : affiner la conf du serveur SSH**

- faites vos plus belles recherches internet pour améliorer la conf de SSH
- par "améliorer" on entend essentiellement ici : augmenter son niveau de sécurité
- le but c'est pas de me rendre 10000 lignes de conf que vous pompez sur internet pour le bonus, mais de vous éveiller à divers aspects de SSH, la sécu ou d'autres choses liées

# II. Service HTTP

## 1. Mise en place

🌞 **Installer le serveur NGINX**

- je vous laisse faire votre recherche internet
- n'oubliez pas de préciser que c'est pour "Rocky 9"

```
[dora@LinuxOne ssh]$ sudo dnf install nginx

Installed:
  nginx-1:1.20.1-13.el9.x86_64             nginx-core-1:1.20.1-13.el9.x86_64
  nginx-filesystem-1:1.20.1-13.el9.noarch  rocky-logos-httpd-90.13-1.el9.noarch

Complete!
```

```
[dora@LinuxOne ssh]$ sudo systemctl enable nginx
Created symlink /etc/systemd/system/multi-user.target.wants/nginx.service → /usr/lib/systemd/system/nginx.service.
```

```
[dora@LinuxOne ssh]$ sudo systemctl start nginx
```

```
[dora@LinuxOne ssh]$ sudo firewall-cmd --permanent --add-service=http
success
```

```
[dora@LinuxOne ssh]$ sudo firewall-cmd --permanent --list-all | grep http
  services: cockpit dhcpv6-client http ssh
```

```
[dora@LinuxOne ssh]$ sudo firewall-cmd --reload
success
```

🌞 **Démarrer le service NGINX**

```
[dora@LinuxOne ssh]$ sudo systemctl enable nginx
Created symlink /etc/systemd/system/multi-user.target.wants/nginx.service → /usr/lib/systemd/system/nginx.service.
```

```
[dora@LinuxOne ssh]$ sudo systemctl start nginx
```

🌞 **Déterminer sur quel port tourne NGINX**

- vous devez filtrer la sortie de la commande utilisée pour n'afficher que les lignes demandées

```
[dora@LinuxOne ssh]$ sudo ss -alnpt | grep nginx
LISTEN 0      511          0.0.0.0:80        0.0.0.0:*    users:(("nginx",pid=11473,fd=6),("nginx",pid=11472,fd=6))
LISTEN 0      511             [::]:80           [::]:*    users:(("nginx",pid=11473,fd=7),("nginx",pid=11472,fd=7))
```

- ouvrez le port concerné dans le firewall

```
[dora@LinuxOne ssh]$ sudo firewall-cmd --add-port=80/tcp --permanent
success
```

```
[dora@LinuxOne ssh]$ sudo firewall-cmd --reload
success
```

```
[dora@LinuxOne ssh]$ sudo firewall-cmd --list-all | grep 80
  ports: 19902/tcp 22/tcp 80/tcp
```

🌞 **Déterminer les processus liés à l'exécution de NGINX**

- vous devez filtrer la sortie de la commande utilisée pour n'afficher que les lignes demandées

```
[dora@LinuxOne ssh]$ ps -ef | grep nginx
root       11472       1  0 15:55 ?        00:00:00 nginx: master process /usr/sbin/nginx
nginx      11473   11472  0 15:55 ?        00:00:00 nginx: worker process
dora       11549    1585  0 16:14 pts/1    00:00:00 grep --color=auto nginx
```

🌞 **Euh wait**

- y'a un serveur Web qui tourne là ?
- bah... visitez le site web ?
  - ouvrez votre navigateur (sur votre PC) et visitez `http://<IP_VM>:<PORT>`
  - vous pouvez aussi (toujours sur votre PC) utiliser la commande `curl` depuis un terminal pour faire une requête HTTP

```
[dora@LinuxOne ~]$ curl http://10.4.1.70:80
```

- dans le compte-rendu, je veux le `curl` (pas un screen de navigateur)
  - utilisez Git Bash si vous êtes sous Windows (obligatoire)
  - vous utiliserez `| head` après le `curl` pour afficher que certaines des premières lignes
  - vous utiliserez une option à cette commande `head` pour afficher les 7 premières lignes de la sortie du `curl`

<br>

  ```
  [dora@LinuxOne ~]$ curl http://10.4.1.70:80 | head -n 7
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
  0     0    0     0    0     0      0      0 --:--:-- --:--:-- --:--:--     0<!doctype html>
<html>
  <head>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <title>HTTP Server Test Page powered by: Rocky Linux</title>
    <style type="text/css">
100  7620  100  7620    0     0   930k      0 --:--:-- --:--:-- --:--:--  930k
curl: (23) Failed writing body
```

## 2. Analyser la conf de NGINX

🌞 **Déterminer le path du fichier de configuration de NGINX**

- faites un `ls -al <PATH_VERS_LE_FICHIER>` pour le compte-rendu

```
[dora@LinuxOne system]$ ls -al /etc/nginx/nginx.conf
-rw-r--r-- 1 root root 2266 Dec  6 11:05 /etc/nginx/nginx.conf

```

🌞 **Trouver dans le fichier de conf**

- les lignes qui permettent de faire tourner un site web d'accueil (la page moche que vous avez vu avec votre navigateur)
  - ce que vous cherchez, c'est un bloc `server { }` dans le fichier de conf
  - vous ferez un `cat <FICHIER> | grep <TEXTE> -A X` pour me montrer les lignes concernées dans le compte-rendu
    - l'option `-A X` permet d'afficher aussi les `X` lignes après chaque ligne trouvée par `grep`

```
[dora@LinuxOne nginx]$ cat nginx.conf | grep server -A 7
    server {
        listen       80;
        listen       [::]:80;
        server_name  _;
        root         /usr/share/nginx/html;
    }

```

- une ligne qui parle d'inclure d'autres fichiers de conf
  - encore un `cat <FICHIER> | grep <TEXTE>`
  - bah ouais, on stocke pas toute la conf dans un seul fichier, sinon ça serait le bordel

```
  [dora@LinuxOne nginx]$ cat nginx.conf | grep conf

include /usr/share/nginx/modules/*.conf;
    [...]
    include /etc/nginx/conf.d/*.conf;
       [...]
        include /etc/nginx/default.d/*.conf;
```

## 3. Déployer un nouveau site web

🌞 **Créer un site web**

- bon on est pas en cours de design ici, alors on va faire simplissime
- créer un sous-dossier dans `/var/www/`

```
[dora@LinuxOne var]$ sudo mkdir www
```

  - par convention, on stocke les sites web dans `/var/www/`
  - votre dossier doit porter le nom `tp2_linux`

```
[dora@LinuxOne www]$ sudo mkdir tp2_linux
```

- dans ce dossier `/var/www/tp2_linux`, créez un fichier `index.html`

```
[dora@LinuxOne tp2_linux]$ sudo nano index.html
```

  - il doit contenir `<h1>MEOW mon premier serveur web</h1>`

```
[dora@LinuxOne tp2_linux]$ cat index.html
<h1>MEOW mon premier serveur web</h1>
```

🌞 **Adapter la conf NGINX**

- dans le fichier de conf principal
  - vous supprimerez le bloc `server {}` repéré plus tôt pour que NGINX ne serve plus le site par défaut
  - redémarrez NGINX pour que les changements prennent effet

```
[dora@LinuxOne nginx]$ sudo systemctl restart nginx
```

- créez un nouveau fichier de conf
  - il doit être nommé correctement
  - il doit être placé dans le bon dossier

```
[dora@LinuxOne nginx]$ sudo nano tp2_linux.conf conf.d/
```


  - c'est quoi un "nom correct" et "le bon dossier" ?
    - bah vous avez repéré dans la partie d'avant les fichiers qui sont inclus par le fichier de conf principal non ?
    - créez votre fichier en conséquence

```
[dora@LinuxOne nginx]$ echo $RANDOM
11209
```

  - redémarrez NGINX pour que les changements prennent effet
  - le contenu doit être le suivant :

```nginx
server {
  # le port choisi devra être obtenu avec un 'echo $RANDOM' là encore
  listen <PORT>;

  root /var/www/tp2_linux;
}
```

```
[dora@LinuxOne nginx]$ sudo cat tp2_linux.conf
server {
  listen <11209>;

  root /var/www/tp2_linux;
}
```

🌞 **Visitez votre super site web**

- toujours avec une commande `curl` depuis votre PC (ou un navigateur)

```
$ curl 10.4.1.70:11209
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100    38  100    38    0     0  24437      0 --:--:-- --:--:-- --:--:-- 38000<h1>MEOW mon premier serveur web</h1>
```

# III. Your own services


## 2. Analyse des services existants

🌞 **Afficher le fichier de service SSH**

- vous pouvez obtenir son chemin avec un `systemctl status <SERVICE>`

```
[dora@LinuxOne nginx]$ systemctl status sshd
● sshd.service - OpenSSH server daemon
     Loaded: loaded (/usr/lib/systemd/system/sshd.service; 
```

- mettez en évidence la ligne qui commence par `ExecStart=`
  - encore un `cat <FICHIER> | grep <TEXTE>`
  - c'est la ligne qui définit la commande lancée lorsqu'on "start" le service

```
[dora@LinuxOne nginx]$ cat /usr/lib/systemd/system/sshd.service | grep ExecStart=
ExecStart=/usr/sbin/sshd -D $OPTIONS
```

   - taper `systemctl start <SERVICE>` ou exécuter cette commande à la main, c'est (presque) pareil


```
[dora@LinuxOne nginx]$sudo systemctl start sshd
```

🌞 **Afficher le fichier de service NGINX**

- mettez en évidence la ligne qui commence par `ExecStart=`

```
[dora@LinuxOne nginx]$ cat /usr/lib/systemd/system/nginx.service | grep ExecStart=
ExecStart=/usr/sbin/nginx
```

## 3. Création de service

🌞 **Créez le fichier `/etc/systemd/system/tp2_nc.service`**

- son contenu doit être le suivant (nice & easy)

```service
[Unit]
Description=Super netcat tout fou

[Service]
ExecStart=/usr/bin/nc -l <PORT>
```

> Vous remplacerez `<PORT>` par un numéro de port random obtenu avec la même méthode que précédemment.

```
[dora@LinuxOne nginx]$ echo $RANDOM
14294
```

```
[dora@LinuxOne nginx]$ sudo cat /etc/systemd/system/tp2_nc.service
[Unit]
Description=Super netcat tout fou

[Service]
ExecStart=/usr/bin/nc -l 14294
```

```
[dora@LinuxOne system]$ sudo firewall-cmd --add-port=14294/tcp --permanent
success
```

🌞 **Indiquer au système qu'on a modifié les fichiers de service**

- la commande c'est `sudo systemctl daemon-reload`

[dora@LinuxOne nginx]$ sudo systemctl daemon-reload


🌞 **Démarrer notre service de ouf**

- avec une commande `systemctl start`

```
[dora@LinuxOne nginx]$ sudo systemctl start tp2_nc.service
```

🌞 **Vérifier que ça fonctionne**

- vérifier que le service tourne avec un `systemctl status <SERVICE>`

```
[dora@LinuxOne system]$ sudo systemctl status tp2_nc.service
● tp2_nc.service - Super netcat tout fou
     Loaded: loaded (/etc/systemd/system/tp2_nc.service; static)
     Active: active (running) since Tue 2022-12-06 11:33:59 CET; 3s ago
   Main PID: 1631 (nc)
      Tasks: 1 (limit: 5905)
     Memory: 784.0K
        CPU: 6ms
     CGroup: /system.slice/tp2_nc.service
             └─1631 /usr/bin/nc -l 14294

Dec 06 11:33:59 LinuxOne systemd[1]: Started Super netcat tout fou.
```

- vérifier que `nc` écoute bien derrière un port avec un `ss`
  - vous filtrerez avec un `| grep` la sortie de la commande pour n'afficher que les lignes intéressantes

```
[dora@LinuxOne system]$ sudo ss -alpnt | grep nc
LISTEN 0      10           0.0.0.0:14294      0.0.0.0:*    users:(("nc",pid=1631,fd=4))                  
LISTEN 0      10              [::]:14294         [::]:*    users:(("nc",pid=1631,fd=3))
```

- vérifer que juste ça marche en vous connectant au service depuis votre PC

ça marche! :)

➜ Si vous vous connectez avec le client, que vous envoyez éventuellement des messages, et que vous quittez `nc` avec un CTRL+C, alors vous pourrez constater que le service s'est stoppé

- bah oui, c'est le comportement de `nc` ça ! 
- le client se connecte, et quand il se tire, ça ferme `nc` côté serveur aussi
- faut le relancer si vous voulez retester !

🌞 **Les logs de votre service**

- mais euh, ça s'affiche où les messages envoyés par le client ? Dans les logs !
- `sudo journalctl -xe -u tp2_nc` pour visualiser les logs de votre service
- `sudo journalctl -xe -u tp2_nc -f ` pour visualiser **en temps réel** les logs de votre service
  - `-f` comme follow (on "suit" l'arrivée des logs en temps réel)
- dans le compte-rendu je veux
  - une commande

  ```
  [dora@LinuxOne system]$ sudo journalctl -xe -u tp2_nc | grep start
  ```

  `journalctl` filtrée avec `grep` qui affiche la ligne qui indique le démarrage du service

```
░░ Subject: A start job for unit tp2_nc.service has finished successfully
░░ A start job for unit tp2_nc.service has finished successfully.
```

  - une commande `journalctl` filtrée avec `grep` qui affiche un message reçu qui a été envoyé par le client

```
[dora@LinuxOne system]$ sudo journalctl -xe -u tp2_nc | grep 1656
Dec 06 12:13:48 LinuxOne nc[1656]: haha
Dec 06 12:14:15 LinuxOne nc[1656]: hehe
```

  - une commande `journalctl` filtrée avec `grep` qui affiche la ligne qui indique l'arrêt du service

  ```
  [dora@LinuxOne system]$ sudo journalctl -xe -u tp2_nc | grep exit
Dec 06 10:24:37 LinuxOne systemd[1]: tp2_nc.service: Main process exited, code=exited, status=2/INVALIDARGUMENT
░░ Subject: Unit process exited
░░ An ExecStart= process belonging to unit tp2_nc.service has exited.
░░ The process' exit code is 'exited' and its exit status is 2.
  ```

🌞 **Affiner la définition du service**

- faire en sorte que le service redémarre automatiquement s'il se termine
  - comme ça, quand un client se co, puis se tire, le service se relancera tout seul
  - ajoutez `Restart=always` dans la section `[Service]` de votre service

```
[dora@LinuxOne system]$ sudo cat /etc/systemd/system/tp2_nc.service
[Unit]
Description=Super netcat tout fou

[Service]
ExecStart=/usr/bin/nc -l 14294

Restart=always
```

  - n'oubliez pas d'indiquer au système que vous avez modifié les fichiers de service :)

```
[dora@LinuxOne system]$ sudo systemctl daemon-reload
```
```
[dora@LinuxOne system]$ sudo systemctl start tp2_nc.service
```