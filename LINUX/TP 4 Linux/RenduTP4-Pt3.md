# Partie 3 : Serveur web

## 2. Install

🖥️ **VM web.tp4.linux**

🌞 **Installez NGINX**

- installez juste NGINX (avec un `dnf install`) et passez à la suite
- référez-vous à des docs en ligne si besoin

```
[dora@web ~]$ sudo dnf install nginx
Installed:
  nginx-1:1.20.1-13.el9.x86_64             nginx-core-1:1.20.1-13.el9.x86_64
  nginx-filesystem-1:1.20.1-13.el9.noarch  rocky-logos-httpd-90.13-1.el9.noarch

Complete!
```

## 3. Analyse

🌞 **Analysez le service NGINX**

- avec une commande `ps`, déterminer sous quel utilisateur tourne le processus du service NGINX

  - utilisez un `| grep` pour isoler les lignes intéressantes

```
[dora@web ~]$ ps -ef | grep nginx
root       11140       1  0 10:20 ?        00:00:00 nginx: master process /usr/sbin/nginx
nginx      11141   11140  0 10:20 ?        00:00:00 nginx: worker process
dora       11149     911  0 10:26 pts/0    00:00:00 grep --color=auto nginx
```

- avec une commande `ss`, déterminer derrière quel port écoute actuellement le serveur web
  - utilisez un `| grep` pour isoler les lignes intéressantes

```
[dora@web ~]$ sudo ss -alnpt | grep nginx
LISTEN 0      511          0.0.0.0:80         0.0.0.0:*    users:(("nginx",pid=11141,fd=6),("nginx",pid=11140,fd=6))
LISTEN 0      511             [::]:80            [::]:*    users:(("nginx",pid=11141,fd=7),("nginx",pid=11140,fd=7))
```

- en regardant la conf, déterminer dans quel dossier se trouve la racine web
  - utilisez un `| grep` pour isoler les lignes intéressantes

```
[dora@web nginx]$ cat nginx.conf | grep root
        root         /usr/share/nginx/html;
```

- inspectez les fichiers de la racine web, et vérifier qu'ils sont bien accessibles en lecture par l'utilisateur qui lance le processus
  - ça va se faire avec un `ls` et les options appropriées

```
[dora@web html]$ ls -al
total 12
drwxr-xr-x. 3 root root  143 Jan  2 10:14 .
drwxr-xr-x. 4 root root   33 Jan  2 10:14 ..
-rw-r--r--. 1 root root 3332 Oct 31 16:35 404.html
-rw-r--r--. 1 root root 3404 Oct 31 16:35 50x.html
drwxr-xr-x. 2 root root   27 Jan  2 10:14 icons
lrwxrwxrwx. 1 root root   25 Oct 31 16:37 index.html -> ../../testpage/index.html
-rw-r--r--. 1 root root  368 Oct 31 16:35 nginx-logo.png
lrwxrwxrwx. 1 root root   14 Oct 31 16:37 poweredby.png -> nginx-logo.png
lrwxrwxrwx. 1 root root   37 Oct 31 16:37 system_noindex_logo.png -> ../../pixmaps/system-noindex-logo.png
```

## 4. Visite du service web

🌞 **Configurez le firewall pour autoriser le trafic vers le service NGINX**

- vous avez reperé avec `ss` dans la partie d'avant le port à ouvrir

```
[dora@web html]$ sudo firewall-cmd --add-port=80/tcp --permanent
success
```

```
[dora@web html]$ sudo firewall-cmd --reload
success
```

🌞 **Accéder au site web**

- avec votre navigateur sur VOTRE PC
  - ouvrez le navigateur vers l'URL : `http://<IP_VM>:<PORT>`
- vous pouvez aussi effectuer des requêtes HTTP depuis le terminal, plutôt qu'avec un navigateur
  - ça se fait avec la commande `curl`
  - et c'est ça que je veux dans le compte-rendu, pas de screen du navigateur :)

```
[dora@web nginx]$ curl -s http://10.4.1.58:80 | head -n 7
<!doctype html>
<html>
  <head>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <title>HTTP Server Test Page powered by: Rocky Linux</title>
    <style type="text/css">
```

🌞 **Vérifier les logs d'accès**

- trouvez le fichier qui contient les logs d'accès, dans le dossier `/var/log`
- les logs d'accès, c'est votre serveur web qui enregistre chaque requête qu'il a reçu
- c'est juste un fichier texte
- affichez les 3 dernières lignes des logs d'accès dans le contenu rendu, avec une commande `tail`

```
[dora@web log]$ sudo cat /var/log/nginx/access.log | tail -n 3
10.4.1.28 - - [02/Jan/2023:10:45:06 +0100] "GET /poweredby.png HTTP/1.1" 200 368 "http://10.4.1.58/" "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/108.0.0.0 Safari/537.36 Edg/108.0.1462.54" "-"
10.4.1.28 - - [02/Jan/2023:10:45:06 +0100] "GET /favicon.ico HTTP/1.1" 404 3332 "http://10.4.1.58/" "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/108.0.0.0 Safari/537.36 Edg/108.0.1462.54" "-"
10.4.1.58 - - [02/Jan/2023:10:49:46 +0100] "GET / HTTP/1.1" 200 7620 "-" "curl/7.76.1" "-"
```

## 5. Modif de la conf du serveur web

🌞 **Changer le port d'écoute**

- une simple ligne à modifier, vous me la montrerez dans le compte rendu
  - faites écouter NGINX sur le port 8080

```
[dora@web nginx]$ sudo cat nginx.conf | grep 8080
        listen       8080;
        listen       [::]:8080;
```

- redémarrer le service pour que le changement prenne effet
  - `sudo systemctl restart nginx`

```
[dora@web nginx]$ sudo systemctl restart nginx
```

  - vérifiez qu'il tourne toujours avec un ptit `systemctl status nginx`

```
[dora@web nginx]$ sudo systemctl status nginx
● nginx.service - The nginx HTTP and reverse proxy server
     Loaded: loaded (/usr/lib/systemd/system/nginx.service; disabled; vendor pr>
     Active: active (running) since Mon 2023-01-02 11:16:25 CET; 39s ago
    Process: 11307 ExecStartPre=/usr/bin/rm -f /run/nginx.pid (code=exited, sta>
    Process: 11308 ExecStartPre=/usr/sbin/nginx -t (code=exited, status=0/SUCCE>
    Process: 11309 ExecStart=/usr/sbin/nginx (code=exited, status=0/SUCCESS)
   Main PID: 11310 (nginx)
      Tasks: 2 (limit: 5905)
     Memory: 1.9M
        CPU: 14ms
     CGroup: /system.slice/nginx.service
             ├─11310 "nginx: master process /usr/sbin/nginx"
             └─11311 "nginx: worker process"
```

- prouvez-moi que le changement a pris effet avec une commande `ss`
  - utilisez un `| grep` pour isoler les lignes intéressantes

```
[dora@web nginx]$ sudo ss -alnpt | grep nginx
LISTEN 0      511          0.0.0.0:8080       0.0.0.0:*    users:(("nginx",pid=11311,fd=6),("nginx",pid=11310,fd=6))
LISTEN 0      511             [::]:8080          [::]:*    users:(("nginx",pid=11311,fd=7),("nginx",pid=11310,fd=7))
```

- n'oubliez pas de fermer l'ancien port dans le firewall, et d'ouvrir le nouveau

```
[dora@web nginx]$ sudo firewall-cmd --add-port=8080/tcp --permanent
success
[dora@web nginx]$ sudo firewall-cmd --remove-port=80/tcp --permanent
success
[dora@web nginx]$ sudo firewall-cmd --reload
success
```

- prouvez avec une commande `curl` sur votre machine que vous pouvez désormais visiter le port 8080

```
[dora@web nginx]$ curl -s http://10.4.1.58:8080 | head -n 7
<!doctype html>
<html>
  <head>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <title>HTTP Server Test Page powered by: Rocky Linux</title>
    <style type="text/css">
```
---

🌞 **Changer l'utilisateur qui lance le service**

- pour ça, vous créerez vous-même un nouvel utilisateur sur le système : `web`
  - référez-vous au [mémo des commandes](../../cours/memos/commandes.md) pour la création d'utilisateur
  - l'utilisateur devra avoir un mot de passe, et un homedir défini explicitement à `/home/web`

```
[dora@web nginx]$ sudo useradd web -m -p aze
```

- modifiez la conf de NGINX pour qu'il soit lancé avec votre nouvel utilisateur
  - utilisez `grep` pour me montrer dans le fichier de conf la ligne que vous avez modifié

```
[dora@web nginx]$ sudo cat nginx.conf | grep user
user web;
    log_format  main  '$remote_addr - $remote_user [$time_local] "$request" '
                      '"$http_user_agent" "$http_x_forwarded_for"';
```

- n'oubliez pas de redémarrer le service pour que le changement prenne effet

```
[dora@web nginx]$ sudo systemctl restart nginx
```

- vous prouverez avec une commande `ps` que le service tourne bien sous ce nouveau utilisateur
  - utilisez un `| grep` pour isoler les lignes intéressantes

```
[dora@web nginx]$ ps -ef | grep web
web        11408   11407  0 11:38 ?        00:00:00 nginx: worker process
dora       11414     911  0 11:39 pts/0    00:00:00 grep --color=auto web
```

---

🌞 **Changer l'emplacement de la racine Web**

- configurez NGINX pour qu'il utilise une autre racine web que celle par défaut
  - avec un `nano` ou `vim`, créez un fichiez `/var/www/site_web_1/index.html` avec un contenu texte bidon
  - dans la conf de NGINX, configurez la racine Web sur `/var/www/site_web_1/`
  - vous me montrerez la conf effectuée dans le compte-rendu, avec un `grep`

```
[dora@web nginx]$ sudo cat nginx.conf | grep root
        root         /var/www/site_web_1;
```

- n'oubliez pas de redémarrer le service pour que le changement prenne effet

```
[dora@web nginx]$ sudo systemctl restart nginx
```

- prouvez avec un `curl` depuis votre hôte que vous accédez bien au nouveau site

```
[dora@web nginx]$ curl http://10.4.1.58:8080
<!DOCTYPE html>
<html>
  <head>
    <title>Titre de la page</title>
  </head>
  <body>
    Contenu de la page
  </body>
</html>
```

## 6. Deux sites web sur un seul serveur

🌞 **Repérez dans le fichier de conf**

- la ligne qui inclut des fichiers additionels contenus dans un dossier nommé `conf.d`
- vous la mettrez en évidence avec un `grep`

```
[dora@web nginx]$ sudo cat nginx.conf | grep conf.d
    include /etc/nginx/conf.d/*.conf;
```


🌞 **Créez le fichier de configuration pour le premier site**

- le bloc `server` du fichier de conf principal, vous le sortez
- et vous le mettez dans un fichier dédié
- ce fichier dédié doit se trouver dans le dossier `conf.d`
- ce fichier dédié doit porter un nom adéquat : `site_web_1.conf`

```
[dora@web nginx]$ sudo cat conf.d/site_web_1.conf
    server {
        listen       8080;
        listen       [::]:8080;
        server_name  _;
        root         /var/www/site_web_1;

     #    Load configuration files for the default server block.
     #   include /etc/nginx/default.d/*.conf;

        error_page 404 /404.html;
        location = /404.html {
        }

        error_page 500 502 503 504 /50x.html;
        location = /50x.html {
        }
    }
```

🌞 **Créez le fichier de configuration pour le deuxième site**

- un nouveau fichier dans le dossier `conf.d`
- il doit porter un nom adéquat : `site_web_2.conf`
- copiez-collez le bloc `server { }` de l'autre fichier de conf
- changez la racine web vers `/var/www/site_web_2/`
- et changez le port d'écoute pour 8888

```
[dora@web nginx]$ sudo cat conf.d/site_web_2.conf
    server {
        listen       8888;
        listen       [::]:8888;
        server_name  _;
        root         /var/www/site_web_2;

     #    Load configuration files for the default server block.
     #   include /etc/nginx/default.d/*.conf;

        error_page 404 /404.html;
        location = /404.html {
        }

        error_page 500 502 503 504 /50x.html;
        location = /50x.html {
        }
    }
```

```
[dora@web nginx]$ sudo firewall-cmd --add-port=8888/tcp --permanent
success
[dora@web nginx]$ sudo firewall-cmd --reload
success
```

🌞 **Prouvez que les deux sites sont disponibles**

- depuis votre PC, deux commandes `curl`
- pour choisir quel site visitez, vous choisissez un port spécifique

```
[dora@web nginx]$ curl http://10.4.1.58:8080
<!DOCTYPE html>
<html>
  <head>
    <title>Titre de la page</title>
  </head>
  <body>
    Contenu de la page
  </body>
</html>
```

```
[dora@web nginx]$ curl http://10.4.1.58:8888
<html>
<head><title>403 Forbidden</title></head>
<body>
<center><h1>403 Forbidden</h1></center>
<hr><center>nginx/1.20.1</center>
</body>
</html>
```
