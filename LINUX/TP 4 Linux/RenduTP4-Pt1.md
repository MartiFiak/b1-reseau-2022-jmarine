# Partie 1 : Partitionnement du serveur de stockage


🌞 **Partitionner le disque à l'aide de LVM**

- créer un *physical volume (PV)* : le nouveau disque ajouté à la VM
- créer un nouveau *volume group (VG)*
  - il devra s'appeler `storage`
  - il doit contenir le PV créé à l'étape précédente
- créer un nouveau *logical volume (LV)* : ce sera la partition utilisable
  - elle doit être dans le VG `storage`
  - elle doit occuper tout l'espace libre

```
[dora@storage dev]$ sudo pvcreate /dev/sdb
  Physical volume "/dev/sdb" successfully created.
```

```
  [dora@storage dev]$ sudo pvs
  Devices file sys_wwid t10.ATA_____VBOX_HARDDISK___________________________VBe1d61327-28e05b53_ PVID WDC6gp3D8KHNfV5BYlvUcezoqhVaQC9E last seen on /dev/sda2 not found.
  PV         VG      Fmt  Attr PSize  PFree
  /dev/sdb   storage lvm2 a--  <2.00g    0
```

```
[dora@storage dev]$ sudo pvdisplay
  Devices file sys_wwid t10.ATA_____VBOX_HARDDISK___________________________VBe1d61327-28e05b53_ PVID WDC6gp3D8KHNfV5BYlvUcezoqhVaQC9E last seen on /dev/sda2 not found.
  --- Physical volume ---
  PV Name               /dev/sdb
  VG Name               storage
  PV Size               2.00 GiB / not usable 4.00 MiB
  Allocatable           yes (but full)
  PE Size               4.00 MiB
  Total PE              511
  Free PE               0
  Allocated PE          511
  PV UUID               PcYqa5-omSi-L0nZ-XCTR-r8sz-e9M2-Ka3o0B

```

```
[dora@storage dev]$ sudo vgcreate storage /dev/sdb
  Volume group "storage" successfully created
```

```
[dora@storage dev]$ sudo vgs
  Devices file sys_wwid t10.ATA_____VBOX_HARDDISK___________________________VBe1d61327-28e05b53_ PVID WDC6gp3D8KHNfV5BYlvUcezoqhVaQC9E last seen on /dev/sda2 not found.
  VG      #PV #LV #SN Attr   VSize  VFree
  storage   1   1   0 wz--n- <2.00g    0
```

```
[dora@storage dev]$ sudo vgdisplay
  Devices file sys_wwid t10.ATA_____VBOX_HARDDISK___________________________VBe1d61327-28e05b53_ PVID WDC6gp3D8KHNfV5BYlvUcezoqhVaQC9E last seen on /dev/sda2 not found.
  --- Volume group ---
  VG Name               storage
  System ID
  Format                lvm2
  Metadata Areas        1
  Metadata Sequence No  2
  VG Access             read/write
  VG Status             resizable
  MAX LV                0
  Cur LV                1
  Open LV               1
  Max PV                0
  Cur PV                1
  Act PV                1
  VG Size               <2.00 GiB
  PE Size               4.00 MiB
  Total PE              511
  Alloc PE / Size       511 / <2.00 GiB
  Free  PE / Size       0 / 0
  VG UUID               8fysrs-5h6m-rhOE-8dex-XVpx-cvqJ-OgZ22m
```

```
[dora@storage dev]$ sudo lvcreate -l 100%FREE storage -n new_storage
  Logical volume "new_storage" created.
```

```
[dora@storage dev]$ sudo lvs
  Devices file sys_wwid t10.ATA_____VBOX_HARDDISK___________________________VBe1d61327-28e05b53_ PVID WDC6gp3D8KHNfV5BYlvUcezoqhVaQC9E last seen on /dev/sda2 not found.
  LV          VG      Attr       LSize  Pool Origin Data%  Meta%  Move Log Cpy%Sync Convert
  new_storage storage -wi-ao---- <2.00g    
````

```
[dora@storage dev]$ sudo lvdisplay
  Devices file sys_wwid t10.ATA_____VBOX_HARDDISK___________________________VBe1d61327-28e05b53_ PVID WDC6gp3D8KHNfV5BYlvUcezoqhVaQC9E last seen on /dev/sda2 not found.
  --- Logical volume ---
  LV Path                /dev/storage/new_storage
  LV Name                new_storage
  VG Name                storage
  LV UUID                qXYMV6-mCPG-YWke-Pkdr-EV1R-KQVv-uWYBvK
  LV Write Access        read/write
  LV Creation host, time storage.tp4.linux, 2022-12-13 10:23:57 +0100
  LV Status              available
  # open                 1
  LV Size                <2.00 GiB
  Current LE             511
  Segments               1
  Allocation             inherit
  Read ahead sectors     auto
  - currently set to     256
  Block device           253:2
```

```
[dora@storage dev]$ lsblk
NAME                  MAJ:MIN RM  SIZE RO TYPE MOUNTPOINTS
sda                     8:0    0  8.3G  0 disk
├─sda1                  8:1    0    1G  0 part /boot
└─sda2                  8:2    0  7.3G  0 part
  ├─rl-root           253:0    0  6.5G  0 lvm  /
  └─rl-swap           253:1    0  856M  0 lvm  [SWAP]
sdb                     8:16   0    2G  0 disk
└─storage-new_storage 253:2    0    2G  0 lvm
sr0                    11:0    1 1024M  0 rom
```

🌞 **Formater la partition**

- vous formaterez la partition en ext4 (avec une commande `mkfs`)
  - le chemin de la partition, vous pouvez le visualiser avec la commande `lvdisplay`
  - pour rappel un *Logical Volume (LVM)* **C'EST** une partition

```
[dora@storage dev]$ sudo !!
sudo mkfs -t ext4 /dev/storage/new_storage
mke2fs 1.46.5 (30-Dec-2021)
Creating filesystem with 523264 4k blocks and 130816 inodes
Filesystem UUID: b7fe6c7c-950d-4f58-a797-d7975ed70430
Superblock backups stored on blocks:
        32768, 98304, 163840, 229376, 294912

Allocating group tables: done
Writing inode tables: done
Creating journal (8192 blocks): done
Writing superblocks and filesystem accounting information: done
```
```
[dora@storage dev]$ sudo mkdir /mnt/storage1
```

```
[dora@storage dev]$ sudo mount /dev/storage/new_storage /mnt/storage1/
```

```
[dora@storage dev]$ sudo lvdisplay | grep Path
  Devices file sys_wwid t10.ATA_____VBOX_HARDDISK___________________________VBe1d61327-28e05b53_ PVID WDC6gp3D8KHNfV5BYlvUcezoqhVaQC9E last seen on /dev/sda2 not found.
  LV Path                /dev/storage/new_storage
```

🌞 **Monter la partition**

- montage de la partition (avec la commande `mount`)
  - la partition doit être montée dans le dossier `/storage`
  - preuve avec une commande `df -h` que la partition est bien montée
    - utilisez un `| grep` pour isoler les lignes intéressantes
  - prouvez que vous pouvez lire et écrire des données sur cette partition
- définir un montage automatique de la partition (fichier `/etc/fstab`)
  - vous vérifierez que votre fichier `/etc/fstab` fonctionne correctement

```
[dora@storage dev]$ df -h | grep storage
/dev/mapper/storage-new_storage  2.0G   24K  1.9G   1% /mnt/storage1
```

```
  [dora@storage storage1]$ cat preuve
Salut c'est cool!
```

```
[dora@storage storage1]$ cat /etc/fstab

[...]

/dev/mapper/rl-root     /                       xfs     defaults        0 0
UUID=1df415e0-bc01-4dfc-b003-f75087cf1730 /boot                   xfs     defaults        0 0
/dev/mapper/rl-swap     none                    swap    defaults        0 0

/dev/mapper/storage-new_storage /mnt/storage1   ext4    defaults        0 0
```

```
[dora@storage mnt]$ sudo umount /mnt/storage1/
```

```
[dora@storage mnt]$ sudo mount -av
/                        : ignored
/boot                    : already mounted
none                     : ignored
mount: /mnt/storage1 does not contain SELinux labels.
       You just mounted a file system that supports labels which does not
       contain labels, onto an SELinux box. It is likely that confined
       applications will generate AVC messages and not be allowed access to
       this file system.  For more details see restorecon(8) and mount(8).
/mnt/storage1            : successfully mounted
```

```
[dora@storage mnt]$ sudo reboot
[dora@storage mnt]$ Connection to 10.4.1.60 closed by remote host.
Connection to 10.4.1.60 closed.
```

