# Module 4 : Monitoring

🌞 **Installer Netdata**

- je vous laisse suivre la doc pour le mettre en place [ou ce genre de lien](https://wiki.crowncloud.net/?How_to_Install_Netdata_on_Rocky_Linux_9)
- vous n'avez PAS besoin d'utiliser le "Netdata Cloud" machin truc. Faites simplement une install locale.
- installez-le sur `web.tp6.linux` et `db.tp6.linux`.

```
[dora@web ~]$ sudo dnf update
Complete!

[dora@web ~]$ sudo dnf install epel-release -y
Complete!

[dora@web ~]$ wget -O /tmp/netdata-kickstart.sh https://my-netdata.io/kickstart.sh && sh /tmp/netdata-kickstart.sh
Complete!
 OK
Successfully installed the Netdata Agent.

[dora@web ~]$ sudo systemctl start netdata
[dora@web ~]$ sudo systemctl enable netdata
[dora@web ~]$ sudo systemctl status netdata
● netdata.service - Real time performance monitoring
     Loaded: loaded (/usr/lib/systemd/system/netdata.service; enabled; vendor preset: d>
     Active: active (running) since Tue 2023-01-17 14:51:49 CET; 59s ago

[dora@web ~]$ sudo firewall-cmd --permanent --add-port=19999/tcp
success

[dora@web ~]$ sudo firewall-cmd --reload
success

[dora@web ~]$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s8 enp0s9
  sources:
  services: cockpit dhcpv6-client ssh
  ports: 80/tcp 3306/tcp 19999/tcp
  protocols:
  forward: yes
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:

[dora@web ~]$ sudo ss -alpnt | grep netdata
LISTEN 0      4096       127.0.0.1:8125       0.0.0.0:*    users:(("netdata",pid=1819,fd=85))
LISTEN 0      4096         0.0.0.0:19999      0.0.0.0:*    users:(("netdata",pid=1819,fd=6))
LISTEN 0      4096           [::1]:8125          [::]:*    users:(("netdata",pid=1819,fd=84))
LISTEN 0      4096            [::]:19999         [::]:*    users:(("netdata",pid=1819,fd=7))
```

```
[dora@db ~]$ sudo dnf update
Complete!

[dora@db ~]$ sudo dnf install epel-release -y
Complete!

[dora@db ~]$ wget -O /tmp/netdata-kickstart.sh https://my-netdata.io/kickstart.sh && sh /tmp/netdata-kickstart.sh
Complete!
 OK
Successfully installed the Netdata Agent.

[dora@db ~]$ sudo systemctl start netdata
[dora@db ~]$ sudo systemctl enable netdata
[dora@db ~]$ sudo systemctl status netdata
● netdata.service - Real time performance monitoring
     Loaded: loaded (/usr/lib/systemd/system/netdata.service; enabled; vendor preset: d>
     Active: active (running) since Tue 2023-01-17 15:00:42 CET; 7min ago

[dora@db ~]$ sudo firewall-cmd --permanent --add-port=19999/tcp
success

[dora@db ~]$ sudo firewall-cmd --reload
success

[dora@db ~]$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s8 enp0s9
  sources:
  services: cockpit dhcpv6-client ssh
  ports: 80/tcp 3306/tcp 19999/tcp
  protocols:
  forward: yes
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:

  [dora@db ~]$ sudo ss -alpnt | grep netdata
LISTEN 0      4096       127.0.0.1:8125       0.0.0.0:*    users:(("netdata",pid=1641,fd=47))
LISTEN 0      4096         0.0.0.0:19999      0.0.0.0:*    users:(("netdata",pid=1641,fd=6))
LISTEN 0      4096           [::1]:8125          [::]:*    users:(("netdata",pid=1641,fd=46))
LISTEN 0      4096            [::]:19999         [::]:*    users:(("netdata",pid=1641,fd=7))

```

🌞 **Une fois Netdata installé et fonctionnel, déterminer :**

- l'utilisateur sous lequel tourne le(s) processus Netdata
- si Netdata écoute sur des ports
- comment sont consultables les logs de Netdata

```
[dora@web ~]$ sudo ss -alpnt | grep netdata
LISTEN 0      4096       127.0.0.1:8125       0.0.0.0:*    users:(("netdata",pid=1819,fd=85))
LISTEN 0      4096         0.0.0.0:19999      0.0.0.0:*    users:(("netdata",pid=1819,fd=6))
LISTEN 0      4096           [::1]:8125          [::]:*    users:(("netdata",pid=1819,fd=84))
LISTEN 0      4096            [::]:19999         [::]:*    users:(("netdata",pid=1819,fd=7))

[dora@web ~]$ sudo ps -ef | grep netdata
netdata     1819       1  0 14:51 ?        00:00:06 /usr/sbin/netdata -P /run/netdata/netdata.pid -D
netdata     1823    1819  0 14:51 ?        00:00:00 /usr/sbin/netdata --special-spawn-server
netdata     1994    1819  0 14:51 ?        00:00:00 bash /usr/libexec/netdata/plugins.d/tc-qos-helper.sh 1
root        1996    1819  0 14:51 ?        00:00:00 /usr/libexec/netdata/plugins.d/ebpf.plugin 1
netdata     2002    1819  0 14:51 ?        00:00:04 /usr/libexec/netdata/plugins.d/apps.plugin 1
netdata     2003    1819  0 14:51 ?        00:00:01 /usr/libexec/netdata/plugins.d/go.d.plugin 1
dora        2528    1192  0 15:11 pts/0    00:00:00 grep --color=auto netdata

[dora@web netdata]$ ls -al
total 484
drwxr-xr-x. 2 netdata root        76 Jan 17 14:51 .
drwxr-xr-x. 9 root    root      4096 Jan 17 14:51 ..
-rw-r--r--. 1 netdata netdata 148948 Jan 17 15:14 access.log
-rw-r--r--. 1 netdata netdata      0 Jan 17 14:51 debug.log
-rw-r--r--. 1 netdata netdata 279047 Jan 17 14:56 error.log
-rw-r--r--. 1 netdata netdata   8739 Jan 17 15:11 health.log
```

```
  [dora@db ~]$ sudo ss -alpnt | grep netdata
LISTEN 0      4096       127.0.0.1:8125       0.0.0.0:*    users:(("netdata",pid=1641,fd=47))
LISTEN 0      4096         0.0.0.0:19999      0.0.0.0:*    users:(("netdata",pid=1641,fd=6))
LISTEN 0      4096           [::1]:8125          [::]:*    users:(("netdata",pid=1641,fd=46))
LISTEN 0      4096            [::]:19999         [::]:*    users:(("netdata",pid=1641,fd=7))

[dora@db ~]$ sudo ps -ef | grep netdata
netdata     1641       1  0 15:00 ?        00:00:02 /usr/sbin/netdata -P /run/netdata/netdata.pid -D
netdata     1643    1641  0 15:00 ?        00:00:00 /usr/sbin/netdata --special-spawn-server
netdata     1818    1641  0 15:00 ?        00:00:00 bash /usr/libexec/netdata/plugins.d/tc-qos-helper.sh 1
netdata     1820    1641  0 15:00 ?        00:00:02 /usr/libexec/netdata/plugins.d/apps.plugin 1
netdata     1821    1641  0 15:00 ?        00:00:00 /usr/libexec/netdata/plugins.d/go.d.plugin 1
root        1828    1641  0 15:00 ?        00:00:00 /usr/libexec/netdata/plugins.d/ebpf.plugin 1

[dora@db ~]$ cd /var/log/netdata/
[dora@db netdata]$ ls -al
total 292
drwxr-xr-x. 2 netdata root        76 Jan 17 15:00 .
drwxr-xr-x. 9 root    root      4096 Jan 17 15:00 ..
-rw-r--r--. 1 netdata netdata      0 Jan 17 15:00 access.log
-rw-r--r--. 1 netdata netdata      0 Jan 17 15:00 debug.log
-rw-r--r--. 1 netdata netdata 279197 Jan 17 15:05 error.log
-rw-r--r--. 1 netdata netdata   8548 Jan 17 15:10 health.log
```

➜ **Vous ne devez PAS utiliser le "Cloud Netdata"**

- lorsque vous accéder à l'interface web de Netdata :
  - vous NE DEVEZ PAS être sur une URL `netdata.cloud`
  - vous DEVEZ visiter l'interface en saisissant l'IP de votre serveur
- l'interface Web tourne surle port 19999 par défaut



🌞 **Configurer Netdata pour qu'il vous envoie des alertes** 

- dans [un salon Discord](https://learn.netdata.cloud/docs/agent/health/notifications/discord) dédié en cas de soucis

```
[dora@web netdata]$ sudo cat /etc/netdata/health_alarm_notify.conf
###############################################################################
# sending discord notifications

# note: multiple recipients can be given like this:
#                  "CHANNEL1 CHANNEL2 ..."

# enable/disable sending discord notifications
SEND_DISCORD="YES"

# Create a webhook by following the official documentation -
# https://support.discordapp.com/hc/en-us/articles/228383668-Intro-to-Webhooks
DISCORD_WEBHOOK_URL="https://discord.com/api/webhooks/1064912326844751903/hEqCznbF_I2CGHmOq9Z0LNzk2Hj1aP9jUSAwPyDIs5tj_QTmAWCYVmDYJOaM3YCtqCSE"

# if a role's recipients are not configured, a notification will be send to
# this discord channel (empty = do not send a notification for unconfigured
# roles):
DEFAULT_RECIPIENT_DISCORD="alarms"
```

```
[dora@db netdata]$ sudo nano /etc/netdata/health_alarm_notify.conf

[dora@db netdata]$ sudo cat /etc/netdata/health_alarm_notify.conf
###############################################################################
# sending discord notifications

# note: multiple recipients can be given like this:
#                  "CHANNEL1 CHANNEL2 ..."

# enable/disable sending discord notifications
SEND_DISCORD="YES"

# Create a webhook by following the official documentation -
# https://support.discordapp.com/hc/en-us/articles/228383668-Intro-to-Webhooks
DISCORD_WEBHOOK_URL="https://discord.com/api/webhooks/1064912326844751903/hEqCznbF_I2CGHmOq9Z0LNzk2Hj1aP9jUSAwPyDIs5tj_QTmAWCYVmDYJOaM3YCtqCSE"

# if a role's recipients are not configured, a notification will be send to
# this discord channel (empty = do not send a notification for unconfigured
# roles):
DEFAULT_RECIPIENT_DISCORD="alarms"
```

🌞 **Vérifier que les alertes fonctionnent**

- en surchargeant volontairement la machine 
- par exemple, effectuez des *stress tests* de RAM et CPU, ou remplissez le disque volontairement
- demandez au grand Internet comme on peut "stress" une machine (c'est le terme technique)

```
[dora@db netdata]$ sudo dnf install stress

Complete!

[dora@db netdata]$ stress --cpu 8 --io 4 --vm 2 --vm-bytes 128M --timeout 120s
stress: info: [2579] dispatching hogs: 8 cpu, 4 io, 2 vm, 0 hdd
```
```
[dora@web netdata]$ sudo dnf install stress

Complete!

[dora@web netdata]$ stress --cpu 8 --io 4 --vm 2 --vm-bytes 128M --timeout 120s
stress: info: [2799] dispatching hogs: 8 cpu, 4 io, 2 vm, 0 hdd
```

```
netdata on web.tp6.linux
BOT
 — Aujourd’hui à 15:39
web.tp6.linux needs attention, mem.sync (synchronization (eBPF)), sync freq = 800603 calls
sync freq = 800603 calls
number of sync() system calls. Every call causes all pending modifications to filesystem metadata and cached file data to be written to the underlying filesystems.
mem.sync
synchronization (eBPF)
Image

web.tp6.linux•Aujourd’hui à 15:38
```

```
Shoukette
#4090

netdata on db.tp6.linux
BOT
 — Aujourd’hui à 15:38
db.tp6.linux needs attention, mem.sync (synchronization (eBPF)), sync freq = 259972 calls
sync freq = 259972 calls
number of sync() system calls. Every call causes all pending modifications to filesystem metadata and cached file data to be written to the underlying filesystems.
mem.sync
synchronization (eBPF)
Image

db.tp6.linux•Aujourd’hui à 15:37
```