# Module 3 : Fail2Ban

🌞 Faites en sorte que :

- si quelqu'un se plante 3 fois de password pour une co SSH en moins de 1 minute, il est ban
- vérifiez que ça fonctionne en vous faisant ban
- utilisez une commande dédiée pour lister les IPs qui sont actuellement ban
- afficher l'état du firewal, et trouver la ligne qui ban l'IP en question
- lever le ban avec une commande liée à fail2ban

```
[dora@db ~]$ sudo dnf install fail2ban
```

```
[dora@db ~]$ sudo cat /etc/fail2ban/jail.local | tail -n 182 | head -n 14
[sshd]

# To use more aggressive sshd modes set filter parameter "mode" in jail.local:
# normal (default), ddos, extra or aggressive (combines all).
# See "tests/files/logs/sshd" or "filter.d/sshd.conf" for usage example and details.
#mode   = normal
enabled = true
port = ssh
filter = sshd
logpath = /var/log/auth.log
maxretry = 3
bantime = 1h
findtime = 1m
```

```
[dora@db ~]$ sudo fail2ban-client status
Status
|- Number of jail:      1
`- Jail list:   sshd
```

```
[dora@db ~]$ sudo iptables -L | grep REJECT
REJECT     all  --  10.105.1.11          anywhere             reject-with icmp-port-unreachable
```

```
[dora@db ~]$ sudo fail2ban-client set sshd unbanip 10.105.1.11
1
```
