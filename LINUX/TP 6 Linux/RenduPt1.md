# Module 1 : Reverse Proxy

# I. Setup

🌞 **On utilisera NGINX comme reverse proxy**

- installer le paquet `nginx`

```
[dora@proxy ~]$ sudo dnf install nginx
```

- démarrer le service `nginx`

```
[dora@proxy ~]$ sudo systemctl start nginx
[dora@proxy ~]$ sudo systemctl status nginx
● nginx.service - The nginx HTTP and reverse proxy server
     Loaded: loaded (/usr/lib/systemd/system/nginx.service; disabled; vendor pr>
     Active: active (running) since Wed 2023-01-11 11:27:34 CET; 8s ago
    Process: 1247 ExecStartPre=/usr/bin/rm -f /run/nginx.pid (code=exited, stat>
    Process: 1248 ExecStartPre=/usr/sbin/nginx -t (code=exited, status=0/SUCCES>
    Process: 1249 ExecStart=/usr/sbin/nginx (code=exited, status=0/SUCCESS)
   Main PID: 1250 (nginx)
      Tasks: 2 (limit: 5905)
     Memory: 1.9M
        CPU: 13ms
     CGroup: /system.slice/nginx.service
             ├─1250 "nginx: master process /usr/sbin/nginx"
             └─1251 "nginx: worker process"

```

- utiliser la commande `ss` pour repérer le port sur lequel NGINX écoute

```
[dora@proxy ~]$ sudo ss -alpnt | grep nginx
LISTEN 0      511          0.0.0.0:80        0.0.0.0:*    users:(("nginx",pid=1251,fd=6),("nginx",pid=1250,fd=6))
LISTEN 0      511             [::]:80           [::]:*    users:(("nginx",pid=1251,fd=7),("nginx",pid=1250,fd=7))
```

- ouvrir un port dans le firewall pour autoriser le trafic vers NGINX

```
[dora@proxy ~]$ sudo firewall-cmd --add-port=80/tcp --permanent
Warning: ALREADY_ENABLED: 80:tcp
success
```

- utiliser une commande `ps -ef` pour déterminer sous quel utilisateur tourne NGINX

```
[dora@proxy ~]$ ps -ef | grep nginx
root        1250       1  0 11:27 ?        00:00:00 nginx: master process /usr/sbin/nginx
nginx       1251    1250  0 11:27 ?        00:00:00 nginx: worker process
dora        1292    1056  0 11:33 pts/0    00:00:00 grep --color=auto nginx

```

- vérifier que le page d'accueil NGINX est disponible en faisant une requête HTTP sur le port 80 de la machine

```
[dora@proxy ~]$ curl -s 10.105.1.16:80 | tail -n 5
      <a href="https://nginx.org">NGINX&trade;</a> is a registered trademark of <a href="https://">F5 Networks, Inc.</a>.
      </footer>

  </body>
</html>
```

🌞 **Configurer NGINX**

- nous ce qu'on veut, c'pas une page d'accueil moche, c'est que NGINX agisse comme un reverse proxy entre les clients et notre serveur Web
- deux choses à faire :
  - créer un fichier de configuration NGINX
    - la conf est dans `/etc/nginx`
    - procédez comme pour Apache : repérez les fichiers inclus par le fichier de conf principal, et créez votre fichier de conf en conséquence

```
[dora@proxy nginx]$ ls -al | grep tp6
-rw-r--r--.  1 root root  938 Jan 16 09:50 tp6.conf
```

```
[dora@proxy nginx]$ cat tp6.conf
server {
    # On indique le nom que client va saisir pour accéder au service
    # Pas d'erreur ici, c'est bien le nom de web, et pas de proxy qu'on veut ici !
    server_name web.tp6.linux;

    # Port d'écoute de NGINX
    listen 80;

    location / {
        # On définit des headers HTTP pour que le proxying se passe bien
        proxy_set_header  Host $host;
        proxy_set_header  X-Real-IP $remote_addr;
        proxy_set_header  X-Forwarded-Proto https;
        proxy_set_header  X-Forwarded-Host $remote_addr;
        proxy_set_header  X-Forwarded-For $proxy_add_x_forwarded_for;

        # On définit la cible du proxying
        proxy_pass http://10.105.1.11:80;
    }

    # Deux sections location recommandés par la doc NextCloud
    location /.well-known/carddav {
      return 301 $scheme://$host/remote.php/dav;
    }

    location /.well-known/caldav {
      return 301 $scheme://$host/remote.php/dav;
    }
}
```

  - NextCloud est un peu exigeant, et il demande à être informé si on le met derrière un reverse proxy
    - y'a donc un fichier de conf NextCloud à modifier
    - c'est un fichier appelé `config.php`
    - la clause à modifier dans ce fichier est `trusted_domains`

```
[dora@web config]$ sudo cat config.php | head -n 10 | tail -n 5
  'trusted_domains' =>
  array (
    0 => '10.105.1.11',
    1 => '10.105.1.16',
  ),
```
    

Référez-vous à monsieur Google pour tout ça :)

Exemple de fichier de configuration minimal NGINX.:

```nginx
server {
    # On indique le nom que client va saisir pour accéder au service
    # Pas d'erreur ici, c'est bien le nom de web, et pas de proxy qu'on veut ici !
    server_name web.tp6.linux;

    # Port d'écoute de NGINX
    listen 80;

    location / {
        # On définit des headers HTTP pour que le proxying se passe bien
        proxy_set_header  Host $host;
        proxy_set_header  X-Real-IP $remote_addr;
        proxy_set_header  X-Forwarded-Proto https;
        proxy_set_header  X-Forwarded-Host $remote_addr;
        proxy_set_header  X-Forwarded-For $proxy_add_x_forwarded_for;

        # On définit la cible du proxying 
        proxy_pass http://<IP_DE_NEXTCLOUD>:80;
    }

    # Deux sections location recommandés par la doc NextCloud
    location /.well-known/carddav {
      return 301 $scheme://$host/remote.php/dav;
    }

    location /.well-known/caldav {
      return 301 $scheme://$host/remote.php/dav;
    }
}
```

➜ **Modifier votre fichier `hosts` de VOTRE PC**

- pour que le service soit joignable avec le nom `web.tp6.linux`
- c'est à dire que `web.tp6.linux` doit pointer vers l'IP de `proxy.tp6.linux`
- autrement dit, pour votre PC :
  - `web.tp6.linux` pointe vers l'IP du reverse proxy
  - `proxy.tp6.linux` ne pointe vers rien
  - taper `http://web.tp6.linux` permet d'accéder au site (en passant de façon transparente par l'IP du proxy)


🌞 **Faites en sorte de**

- rendre le serveur `web.tp6.linux` injoignable
- sauf depuis l'IP du reverse proxy
- en effet, les clients ne doivent pas joindre en direct le serveur web : notre reverse proxy est là pour servir de serveur frontal
- **comment ?** Je vous laisser là encore chercher un peu par vous-mêmes (hint : firewall)

```
[dora@web ~]$ sudo firewall-cmd --set-default-zone drop
success

[dora@web ~]$ sudo firewall-cmd --permanent --zone=drop --change-source=10.105.1.1
6/24
success

[dora@web ~]$ sudo firewall-cmd --permanent --zone=drop --change-source=10.105.1.16
success

[dora@web ~]$ sudo firewall-cmd --reload
success

[dora@web ~]$ sudo firewall-cmd --list-all
drop (active)
  target: DROP
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources: 10.105.1.16/24 10.105.1.16
  services:
  ports:
  protocols:
  forward: yes
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
```

🌞 **Une fois que c'est en place**

- faire un `ping` manuel vers l'IP de `proxy.tp6.linux` fonctionne
- faire un `ping` manuel vers l'IP de `web.tp6.linux` ne fonctionne pas

```
PS C:\Users\ZerBr> ping 10.105.1.16

Envoi d’une requête 'Ping'  10.105.1.16 avec 32 octets de données :
Réponse de 10.105.1.16 : octets=32 temps<1ms TTL=64
Réponse de 10.105.1.16 : octets=32 temps<1ms TTL=64

Statistiques Ping pour 10.105.1.16:
    Paquets : envoyés = 2, reçus = 2, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 0ms, Maximum = 0ms, Moyenne = 0ms
```

```
PS C:\Users\ZerBr> ping 10.105.1.11

Envoi d’une requête 'Ping'  10.105.1.11 avec 32 octets de données :
Délai d’attente de la demande dépassé.

Statistiques Ping pour 10.105.1.11:
    Paquets : envoyés = 1, reçus = 0, perdus = 1 (perte 100%),
```

# II. HTTPS

🌞 **Faire en sorte que NGINX force la connexion en HTTPS plutôt qu'HTTP**
