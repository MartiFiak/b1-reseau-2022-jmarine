#!/bin/bash
echo $ /srv/idcard/idcard.sh

echo "Machine name :$(hostnamectl | grep hostname | cut -d":" -f2)"
source /etc/os-release
echo "OS ${NAME} and kernel version is $(uname -sr)"
echo "IP : $(ip a | grep inet | grep global | cut -b 10-21)"
echo "RAM : $(free -h | grep Mem | cut -b 40-44) memory available on $(free -h | grep Mem | cut -b 16-20) total memory"
echo "Disk : $(df -h | grep root | tr -s ' ' | cut -d' ' -f 4) space left"
echo "Top 5 processes by RAM usage :"
for i in $(seq 1 5)
do
  echo "        - $(ps -eo cmd=,%mem= --sort=-%mem | sed -n ${i}p)"
done
echo "Listening ports :"

ss_op="$(sudo ss -alnptu4H | tr -s ' ')"
while read soso
do
  port=$(echo "$soso" | cut -d' ' -f5 | cut -d':' -f2)
  program=$(echo "$soso" | cut -d'"' -f2)
  type=$(echo "$soso" | cut -d' ' -f1)
  echo "- $port $type : $program"
done <<< "${ss_op}"

curl --silent -o chat.jpg https://cataas.com/cat
echo "Here is your random cat : $(ls chat.jpg)"

