#!/bin/bash

while true
do

if [[ -s /srv/yt/urls.txt ]]
then
  url=$(head -n1 /srv/yt/urls.txt)
else
  exit
fi

$(sed -i '1d' /srv/yt/urls.txt)

title=$(youtube-dl -e "${url}")

if [[ $(youtube-dl -s "${url}" 2> /dev/null) ]]
then
  :
else
  echo "ERROR : not working..."
  exit
fi

if [[ $(find /var/log/yt/ 2> /dev/null) ]]
then
 :
else
  echo "[/var/log/yt] dir file didn't exist."
  exit
fi

if [[ $(find /srv/yt/downloads/ 2> /dev/null) ]]
then
  :
else
  echo "[/srv/yt/downloads] dir didn't exist."
  exit
fi

$(youtube-dl --write-description -o '/srv/yt/downloads/%(title)s/%(title)s.%(ext)s' ${url}"" > /dev/null)

path="/srv/yt/downloads/"${title}/"$(youtube-dl "${url}" --get-filename)"

echo "Video "${url}" was downloaded."
echo "File path : "${path}""

log=$(echo "["$(date +%y/%m/%d) $(date +%T)"] Video "${url}" was dowloaded. File path : "${path}"")
echo ${log} >> /var/log/yt/download.log

done
sleep 1