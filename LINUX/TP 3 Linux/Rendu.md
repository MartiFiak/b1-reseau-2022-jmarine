# TP 3 : We do a little scripting

# 0. Un premier script

# I. Script carte d'identité

## Rendu

📁 **Fichier `/srv/idcard/idcard.sh`**

🌞 **Vous fournirez dans le compte-rendu**, en plus du fichier, **un exemple d'exécution avec une sortie**, dans des balises de code.

``` -c
[dora@TP3 idcard]$ ./idcard.sh
$ /srv/idcard/idcard.sh
Machine name : TP3
OS Rocky Linux and kernel version is Linux 5.14.0-70.26.1.el9_0.x86_64
IP : 10.4.1.51/24
10.0.4.15/24
RAM : 706Mi memory available on 960Mi total memory
Disk : 5.4G space left
Top 5 processes by RAM usage :
        - /usr/bin/python3 -s /usr/sb  4.1
        - /usr/sbin/NetworkManager --  1.9
        - /usr/lib/systemd/systemd --  1.5
        - /usr/lib/systemd/systemd --  1.3
        - /usr/lib/systemd/systemd-ud  1.2
Listening ports :
- 323 udp : chronyd
- 22 tcp : sshd
Here is your random cat : chat.jpg
```

# II. Script youtube-dl

## Rendu

📁 **Le script `/srv/yt/yt.sh`**

📁 **Le fichier de log `/var/log/yt/download.log`**, avec au moins quelques lignes

🌞 Vous fournirez dans le compte-rendu, en plus du fichier, **un exemple d'exécution avec une sortie**, dans des balises de code.

```-c
[dora@TP3 yt]$ /srv/yt/yt.sh https://www.youtube.com/watch?v=a8DM-tD9w2I
Video https://www.youtube.com/watch?v=a8DM-tD9w2I was downloaded.
File path : /srv/yt/downloads/1 SECOND VIDEO (NOT CLICKBAIT)/1 SECOND VIDEO (NOT CLICKBAIT)-a8DM-tD9w2I.mp4
```

```-c
[dora@TP3 yt]$ cat download.log
[22/12/12 19:19:46] Video https://www.youtube.com/watch?v=a8DM-tD9w2I was dowloaded. File path : /srv/yt/downloads/1 SECOND VIDEO (NOT CLICKBAIT)/1 SECOND VIDEO (NOT CLICKBAIT)-a8DM-tD9w2I.mp4
[22/12/12 19:21:58] Video https://www.youtube.com/watch?v=1O0yazhqaxs was dowloaded. File path : /srv/yt/downloads/3 Second Video/3 Second Video-1O0yazhqaxs.webm
[22/12/12 19:22:56] Video https://www.youtube.com/watch?v=YKsQJVzr3a8 was dowloaded. File path : /srv/yt/downloads/Funniest 5 Second Video Ever!/Funniest 5 Second Video Ever!-YKsQJVzr3a8.mp4
```

# III. MAKE IT A SERVICE

## Rendu

📁 **Le script `/srv/yt/yt-v2.sh`**

📁 **Fichier `/etc/systemd/system/yt.service`**

🌞 Vous fournirez dans le compte-rendu, en plus des fichiers :

- un `systemctl status yt` quand le service est en cours de fonctionnement

```-c
[dora@TP3 system]$ systemctl status yt
× yt.service - YTdemo service
     Loaded: loaded (/etc/systemd/system/yt.service; enabled; vendor preset: disabled)
     Active: failed (Result: exit-code) since Tue 2022-12-13 00:22:37 CET; 5s ago
    Process: 3063 ExecStart=/binbash/srv/yt/yt-v2.sh (code=exited, status=203/EXEC)
   Main PID: 3063 (code=exited, status=203/EXEC)
        CPU: 2ms

Dec 13 00:22:37 TP3 systemd[1]: Started YTdemo service.
Dec 13 00:22:37 TP3 systemd[3063]: yt.service: Failed to locate executable /binbash/srv/yt/yt-v2.sh: No such fil>
Dec 13 00:22:37 TP3 systemd[3063]: yt.service: Failed at step EXEC spawning /binbash/srv/yt/yt-v2.sh: No such fi>
Dec 13 00:22:37 TP3 systemd[1]: yt.service: Main process exited, code=exited, status=203/EXEC
Dec 13 00:22:37 TP3 systemd[1]: yt.service: Failed with result 'exit-code'.
```

- un extrait de `journalctl -xe -u yt`

```-c
[dora@TP3 system]$ journalctl -xe -u yt
░░
░░ The job identifier is 2511.
Dec 13 00:22:37 TP3 systemd[3063]: yt.service: Failed to locate executable /binbash/srv/yt/yt-v2.sh: No such fil>░░ Subject: Process /binbash/srv/yt/yt-v2.sh could not be executed
░░ Defined-By: systemd
░░ Support: https://access.redhat.com/support
░░
░░ The process /binbash/srv/yt/yt-v2.sh could not be executed and failed.
░░
░░ The error number returned by this process is ERRNO.
Dec 13 00:22:37 TP3 systemd[3063]: yt.service: Failed at step EXEC spawning /binbash/srv/yt/yt-v2.sh: No such fi>░░ Subject: Process /binbash/srv/yt/yt-v2.sh could not be executed
░░ Defined-By: systemd
░░ Support: https://access.redhat.com/support
░░
░░ The process /binbash/srv/yt/yt-v2.sh could not be executed and failed.
░░
░░ The error number returned by this process is ERRNO.
Dec 13 00:22:37 TP3 systemd[1]: yt.service: Main process exited, code=exited, status=203/EXEC
░░ Subject: Unit process exited
░░ Defined-By: systemd
░░ Support: https://access.redhat.com/support
░░
░░ An ExecStart= process belonging to unit yt.service has exited.
░░
░░ The process' exit code is 'exited' and its exit status is 203.
Dec 13 00:22:37 TP3 systemd[1]: yt.service: Failed with result 'exit-code'.
░░ Subject: Unit failed
░░ Defined-By: systemd
░░ Support: https://access.redhat.com/support
░░
░░ The unit yt.service has entered the 'failed' state with result 'exit-code'.
 ESCOC
```

> Hé oui les commandes `journalctl` fonctionnent sur votre service pour voir les logs ! Et vous devriez constater que c'est vos `echo` qui pop. En résumé, **le STDOUT de votre script, c'est devenu les logs du service !**
