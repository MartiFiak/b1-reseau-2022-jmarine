# Partie 1 : Mise en place et maîtrise du serveur Web

## 1. Installation

🌞 **Installer le serveur Apache**

- paquet `httpd`

```
[dora@web ~]$ sudo dnf install httpd
```

- la conf se trouve dans `/etc/httpd/`
  - le fichier de conf principal est `/etc/httpd/conf/httpd.conf`
  - je vous conseille **vivement** de virer tous les commentaire du fichier, à défaut de les lire, vous y verrez plus clair
    - avec `vim` vous pouvez tout virer avec `:g/^ *#.*/d`

🌞 **Démarrer le service Apache**

- le service s'appelle `httpd` (raccourci pour `httpd.service` en réalité)
  - démarrez-le

```
[dora@web ~]$ sudo systemctl start httpd
[dora@web ~]$ sudo systemctl status httpd
● httpd.service - The Apache HTTP Server
     Loaded: loaded (/usr/lib/systemd/system/httpd.service; disabled; vendor pr>
     Active: active (running) since Tue 2023-01-03 15:10:12 CET; 3s ago
```

  - faites en sorte qu'Apache démarre automatiquement au démarrage de la machine
    - ça se fait avec une commande `systemctl` référez-vous au mémo

```
[dora@web ~]$ sudo systemctl enable httpd
Created symlink /etc/systemd/system/multi-user.target.wants/httpd.service → /usr/lib/systemd/system/httpd.service.
[dora@web ~]$ sudo systemctl is-enabled httpd
enabled
```

  - ouvrez le port firewall nécessaire
    - utiliser une commande `ss` pour savoir sur quel port tourne actuellement Apache
    - une portion du mémo commandes est dédiée à `ss`

    ```
    [dora@web ~]$ sudo ss -alpn | grep httpd
u_str LISTEN 0      100                   /etc/httpd/run/cgisock.1724 23498                  * 0     users:(("httpd",pid=1725,fd=4))                            
u_dgr UNCONN 0      0                                               * 23481                  * 0     users:(("httpd",pid=1728,fd=8),("httpd",pid=1727,fd=8),("httpd",pid=1726,fd=8),("httpd",pid=1724,fd=8))
tcp   LISTEN 0      511                                             *:80                     *:*     users:(("httpd",pid=1728,fd=4),("httpd",pid=1727,fd=4),("httpd",pid=1726,fd=4),("httpd",pid=1724,fd=4))
[dora@web ~]$ sudo ss -alnpt | grep httpd
LISTEN 0      511                *:80              *:*    users:(("httpd",pid=1728,fd=4),("httpd",pid=1727,fd=4),("httpd",pid=1726,fd=4),("httpd",pid=1724,fd=4))
    ```


🌞 **TEST**

- vérifier que le service est démarré

```
[dora@web ~]$ sudo systemctl is-active httpd
active
```

- vérifier qu'il est configuré pour démarrer automatiquement

```
[dora@web ~]$ sudo systemctl is-enabled httpd
enabled
```

- vérifier avec une commande `curl localhost` que vous joignez votre serveur web localement

```
[dora@web ~]$ curl -s localhost | head -n 3
<!doctype html>
<html>
  <head>
```

- vérifier depuis votre PC que vous accéder à la page par défaut
  - avec votre navigateur (sur votre PC)
  - avec une commande `curl` depuis un terminal de votre PC (je veux ça dans le compte-rendu, pas de screen)

```
  ZerBr@MartyFiakoTron MINGW64 ~ (master)
$ curl -s 10.105.1.11 | head -n 3
<!doctype html>
<html>
  <head>
```

## 2. Avancer vers la maîtrise du service

🌞 **Le service Apache...**

- affichez le contenu du fichier `httpd.service` qui contient la définition du service Apache

```
[dora@web ~]$ sudo cat /usr/lib/systemd/system/httpd.service | head -n 22 | tail -n 3
[Service]
Type=notify
Environment=LANG=C
```

🌞 **Déterminer sous quel utilisateur tourne le processus Apache**

- mettez en évidence la ligne dans le fichier de conf principal d'Apache (`httpd.conf`) qui définit quel user est utilisé

```
[dora@web ~]$ sudo cat /etc/httpd/conf/httpd.conf | grep User
User apache
```

- utilisez la commande `ps -ef` pour visualiser les processus en cours d'exécution et confirmer que apache tourne bien sous l'utilisateur mentionné dans le fichier de conf
  - filtrez les infos importantes avec un `| grep`

```
[dora@web ~]$ ps -ef | grep apache
apache      1725    1724  0 15:10 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache      1726    1724  0 15:10 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache      1727    1724  0 15:10 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache      1728    1724  0 15:10 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
dora        2098     853  0 15:42 pts/0    00:00:00 grep --color=auto apache
```

- la page d'accueil d'Apache se trouve dans `/usr/share/testpage/`
  - vérifiez avec un `ls -al` que tout son contenu est **accessible en lecture** à l'utilisateur mentionné dans le fichier de conf

```
[dora@web testpage]$ ls -al | grep index
-rw-r--r--.  1 root root 7620 Jul 27 20:05 index.html
```


🌞 **Changer l'utilisateur utilisé par Apache**

- créez un nouvel utilisateur
  - pour les options de création, inspirez-vous de l'utilisateur Apache existant
    - le fichier `/etc/passwd` contient les informations relatives aux utilisateurs existants sur la machine
    - servez-vous en pour voir la config actuelle de l'utilisateur Apache par défaut (son homedir et son shell en particulier)
- modifiez la configuration d'Apache pour qu'il utilise ce nouvel utilisateur
  - montrez la ligne de conf dans le compte rendu, avec un `grep` pour ne montrer que la ligne importante
- redémarrez Apache
- utilisez une commande `ps` pour vérifier que le changement a pris effet
  - vous devriez voir un processus au moins qui tourne sous l'identité de votre nouvel utilisateur

```
  [dora@web ~]$ sudo adduser newuser
```

```
[dora@web ~]$ sudo cat /etc/httpd/conf/httpd.conf | grep User
User newuser
```

```
[dora@web ~]$ ps -ef | grep newuser
newuser     2378    2377  0 15:52 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
newuser     2379    2377  0 15:52 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
newuser     2380    2377  0 15:52 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
newuser     2381    2377  0 15:52 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
dora        2611     853  0 15:54 pts/0    00:00:00 grep --color=auto newuser
```

🌞 **Faites en sorte que Apache tourne sur un autre port**

- modifiez la configuration d'Apache pour lui demander d'écouter sur un autre port de votre choix
  - montrez la ligne de conf dans le compte rendu, avec un `grep` pour ne montrer que la ligne importante

```
[dora@web ~]$ sudo cat /etc/httpd/conf/httpd.conf | grep 808
Listen 808
```

- ouvrez ce nouveau port dans le firewall, et fermez l'ancien

```
[dora@web ~]$ sudo firewall-cmd --add-port=808/tcp --permanent
success
[dora@web ~]$ sudo firewall-cmd --reload
success
```

- redémarrez Apache

```
[dora@web ~]$ sudo systemctl restart httpd
```

- prouvez avec une commande `ss` que Apache tourne bien sur le nouveau port choisi

```
[dora@web ~]$ sudo ss -alnpt | grep httpd
LISTEN 0      511                *:808             *:*    users:(("httpd",pid=2871,fd=4),("httpd",pid=2870,fd=4),("httpd",pid=2869,fd=4),("httpd",pid=2866,fd=4))
```

- vérifiez avec `curl` en local que vous pouvez joindre Apache sur le nouveau port

```
[dora@web ~]$ curl -s localhost:808 | head -n 5
<!doctype html>
<html>
  <head>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
```

- vérifiez avec votre navigateur que vous pouvez joindre le serveur sur le nouveau port



📁 **Fichier `/etc/httpd/conf/httpd.conf`**
