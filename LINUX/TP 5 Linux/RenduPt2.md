# Partie 2 : Mise en place et maîtrise du serveur de base de données

🌞 **Install de MariaDB sur `db.tp5.linux`**

- déroulez [la doc d'install de Rocky](https://docs.rockylinux.org/guides/database/database_mariadb-server/)
- je veux dans le rendu **toutes** les commandes réalisées
- faites en sorte que le service de base de données démarre quand la machine s'allume
  - pareil que pour le serveur web, c'est une commande `systemctl` fiez-vous au mémo

  ```
  [dora@db ~]$ sudo dnf install mariadb-server

[dora@db ~]$ sudo systemctl enable mariadb
Created symlink /etc/systemd/system/mysql.service → /usr/lib/systemd/system/mariadb.service.
Created symlink /etc/systemd/system/mysqld.service → /usr/lib/systemd/system/mariadb.service.
Created symlink /etc/systemd/system/multi-user.target.wants/mariadb.service → /usr/lib/systemd/system/mariadb.service.

[dora@db ~]$ sudo systemctl start mariadb


[dora@db ~]$ sudo mysql_secure_installation
```

🌞 **Port utilisé par MariaDB**

- vous repérerez le port utilisé par MariaDB avec une commande `ss` exécutée sur `db.tp5.linux`
  - filtrez les infos importantes avec un `| grep`
- il sera nécessaire de l'ouvrir dans le firewall

```
[dora@db ~]$ sudo ss -aplnt | grep maria
LISTEN 0      80                 *:3306            *:*    users:(("mariadbd",pid=3801,fd=19))

[dora@db ~]$ sudo firewall-cmd --add-port=3306/tcp --permanent
success
[dora@db ~]$ sudo firewall-cmd --reload
success
```

🌞 **Processus liés à MariaDB**

- repérez les processus lancés lorsque vous lancez le service MariaDB
- utilisz une commande `ps`
  - filtrez les infos importantes avec un `| grep`

```
[dora@db ~]$ sudo ps -ef | grep mariadb
mysql       3801       1  0 16:33 ?        00:00:00 /usr/libexec/mariadbd --basedir=/usr
dora        3938     852  0 16:41 pts/0    00:00:00 grep --color=auto mariadb
```
